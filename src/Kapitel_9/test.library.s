*
*	Kapitel 9
*	Quelltext der "test.library"
*

ExecBase	=	4
OpenLib 	=	-552
CloseLib	=	-414
Output		=	-60
Write		=	-48
Remove		=	-252
FreeMem		=	-210

Start:	move.l	ExecBase,a6	; Dos-Library �ffnen
	lea	DosName,a1
	moveq 	#0,d0
	jsr	OpenLib(a6)
	move.l	d0,a6
	beq	DosError

	jsr	Output(a6)	; Ausgabekanal ermitteln
	move.l	d0,d1

	move.l	#TextA,d2	; Text ausgeben
	move.l	#TextE-TextA,d3
	jsr	Write(a6)
 
	move.l	a6,a1		; Dos-Library schlie�en
	move.l	ExecBase,a6
	jsr	CloseLib(a6)

DosError:
	moveq  #0,d0		; Fehlercode = 0
	rts

TextA:	dc.b	10,"�� TestLibrary Version 0.1 (Ron) ��",10,10
TextE:
	even

;
;	Anfang der eigentlichen Library
;


LibResident:
	dc.w	$4AFC			; Resident-Sturktur zur 
	dc.l	LibResident		; Initialisierung der
	dc.l	EndResident		; Library
	dc.b	%10000000
	dc.b	1
	dc.b	9
	dc.b	0
	dc.l	LibName
	dc.l	LibIDString
	dc.l	LibInitData

LibName:	dc.b	"test.library",0
	even
LibIDString:	dc.b	"MY-Library v0.1",0
	even

LibInitData:
	dc.l	42	; LibSize (34) + SegList (4) + DosBase (4) = 42
	dc.l	FuncTab
	dc.l	DataTab
	dc.l	LibInitRout

FuncTab:
	dc.l	Open		;
	dc.l	Close		; Library-
	dc.l	Expunge		; Standardfunktionen
	dc.l	ExtFunc		;

	dc.l	WriteLn		; Spezialfunktion (Offset -30)

	dc.l	-1

DataTab:
	dc.b	%11100000,0	; Init-Struct-Tabelle zur initialisierung
	dc.w	8		; der Library-Struktur
	dc.b	9,0

	dc.b	%11000000,0
	dc.w	10
	dc.l	LibName

	dc.b	%11100000,0
	dc.w	14
	dc.b	6,0

	dc.b	%11000000,0
	dc.w	20
	dc.w	3,4

	dc.l	0

DosName:	dc.b "dos.library",0
	even


; Nun folgt die Initialisierungsroutine, die von MakeLibrary aufgerufen wird

LibInitRout:
	movem.l	a4-a6,-(a7)

	move.l	d0,a5		; Library-Basis nach a4

	move.l	a0,34(a5)	; Segment-Liste eintragen

	move.l	ExecBase,a6
	moveq	#0,d0
	lea	DosName(pc),a1
	jsr	OpenLib(a6)
	move.l	d0,38(a5)	; DosBase eintragen
	beq	LibInitEnd

	move.l	a5,d0		; Lirary-Basis �bergeben
LibInitEnd:
	movem.l	(a7)+,a4-a6
	rts

; Die Open-Funktion wird von der OpenLibrary-Routine angesprungen

Open:	
	bclr	#3,14(a6)	; ExpungBit = 0
	addq.w	#1,32(a6)	; OpenCnt ++
	move.l	a6,d0		; BasePtr zur�ckgeben
	rts

; Die Close-Funktion wird von der CloseLibrry-Routine ausgef�hrt

Close:	moveq	#0,d0		; Zeiger auf Segment-Liste = 0

	subq.w	#1,32(a6)	; OpenCnt --
	bne	CloseEnd	; wars der letzte ?

	btst	#3,14(a6)	; ExpungBit = 1 ?
	beq	CloseEnd

	bsr	Expunge		; entfernen

CloseEnd:
	rts

; Die Expunge-Funktion wird eigentlich von der RemLibrary-Routine auf-
; gerufen. Doch kann es auch vorkommen, da� sie von der Close-Funktion
; angesprungen wird.

Expunge:
	movem.l	d1-d2/a1-a6,-(a7)

	tst.w	32(a6)		; gibt es noch Benutzer ?
	beq	ExpungeBranch	; Nein!

	moveq	#0,d0		; Zeiger auf Segment-Liste = 0
	bset	#3,14(a6)	; Expunge-Bit setzen
	bra	ExpungeEnd	; Ende

ExpungeBranch:
	move.l	a6,a4		; Dos-Library schlie�en
	move.l	38(a4),a1
	move.l	ExecBase,a6
	jsr	CloseLib(a6)

	move.l	a4,a1		; Library aus LibList entfernen
	jsr	Remove(a6)	; Remove

	move.l	34(a4),d2	; SegmentList retten

	moveq	#0,d0		; Library-Struktur-Speicher freigeben
	move.w	16(a4),d0
	move.l	a4,a1
	sub.l	d0,a1
	add.w	18(a4),d0
	jsr	FreeMem(a6)	; FreeMem

	move.l	d2,d0		; SegmentList zur�ckgeben

ExpungeEnd:
	movem.l	(a7)+,d1-d2/a1-a6
	rts

; Die ExtFunc wird zur Zeit nicht benutzt.

ExtFunc:
	moveq	#0,d0		; R�ckgabeparameter auf Null setzen
	rts

; Jetzt kommt unsere eigene Funktion

WriteLn:
	movem.l	a0/a6,-(a7)	; Zeiger auf Text retten

	move.l	a6,a5		; DosBase auslesen
	move.l	38(a6),a6

	jsr	Output(a6)	; Ausgabekanal ermitteln
	move.l	d0,d1

	move.l	(a7)+,a0	; L�nge der Zeichenkette ermitteln
	move.l	a0,d2
	move.l	#-1,d3
WLLoop:
	addq.l	#1,d3
	tst.b	(a0)+
	bne	WLLoop
	
	jsr	Write(a6)	; Text ausgeben

	move.l	(a7)+,a6	; BasePtr restaurieren
	rts

EndResident:

; Ende der Library


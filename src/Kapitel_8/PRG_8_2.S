*
* Kapitel 8
* Demonstrationsprogramm f�r die Alloc-/FreeRemember Funktionen
*

ExecBase	=	4
OldOpenLib	=	-408
CloseLib	=	-414
AllocRemember	=	-396
FreeRemember	=	-408

Start:
	move.l	ExecBase,a6

	lea	IntName,a1		; Intuition-Library �ffnen
	jsr	OldOpenLib(a6)
	move.l	d0,IntBase
	move.l	d0,a6
					; 1. Block belegen
	lea	RememberKey,a0		;    RememberKey
	move.l	#512,d0			;    Gr��e
	move.l	#0,d1			;    Typ
	jsr	AllocRemember(a6)	;    Speicher belegen
	move.l	d0,MemPtr1		;    Ptr speichern

					; 2. Block belegen
	lea	RememberKey,a0		;    RememberKey
	move.l	#312,d0			;    Gr��e
	move.l	#0,d1			;    Typ
	jsr	AllocRemember(a6)	;    Speicher belegen
	move.l	d0,MemPtr2		;    Ptr speichern

	; ... Programm ...

	moveq	#-1,d0			; ReallyForget!
	move.l	RememberKey,a0		; RememberKey nach a0
	jsr	FreeRemember(a6)	; Speicher freigeben

	move.l	ExecBase,a6		; Library schlie�en
	move.l	IntBase,a1
	jsr	CloseLib(a6)
	rts

* Datenbereich

IntName:	dc.b	"intuition.library",0
	even
IntBase:	dc.l	0

RememberKey:	dc.l	0
MemPtr1:	dc.l	0
MemPtr2:	dc.l	0

* Ende

*
* Kapitel 5
* Demonstrationsprogramm f�r Men�s
*

ExecBase		=	4
OldOpenLib		=	-408
CloseLib		=	-414
GetMsg			=	-372
ReplyMsg		=	-378
WaitPort		=	-384
OpenWindow		=	-204
CloseWindow		=	-72
SetMenuStrip		=	-264

Start:
	move.l	ExecBase,a6
	lea	IntName,a1
	jsr	OldOpenLib(a6)
	move.l	d0,IntBase
	beq	IntError

	move.l	IntBase,a6
	lea	WindowArgs,a0
	jsr	OpenWindow(a6)
	move.l	d0,WindowHD
	beq	WinError

	move.l	WindowHD,a0
	lea	Menu1,a1
	jsr	SetMenuStrip(a6)	

	move.l	WindowHD,a0
	move.l	86(a0),UPort

MessageLoop:
	move.l	ExecBase,a6

	move.l	UPort,a0
	jsr	GetMsg(a6)
	move.l	d0,Message
	bne	MessageBranch

	move.l	UPort,a0
	jsr	WaitPort(a6)
	bra	MessageLoop

Exit:
	move.l	IntBase,a6
	move.l	WindowHD,a0
	jsr	CloseWindow(a6)

WinError:
	move.l	ExecBase,a6
	move.l	IntBase,a1
	jsr	CloseLib(a6)

IntError:
	rts

*

MessageBranch:
	move.l	Message,a0
	move.l	20(a0),Class
	move.w	24(a0),Code

	move.l	Message,a1
	jsr	ReplyMsg(a6)

	cmp.l	#$100,Class
	beq	MenuLoop

	bra	MessageLoop

MenuLoop:
	lea	BranchTabelle,a1

	move.w	Code,d0
	cmp.w	#$FFFF,d0
	beq	MessageLoop

	and.l	#%11111,d0
	lsl.l	#2,d0
	add.l	d0,a1
	move.l	(a1),a1

	move.w	Code,d1
	lsr.l	#5,d1
	and.l	#%111111,d1
	lsl.l	#2,d1
	add.l	d1,a1
	move.l	(a1),a1

	jmp	(a1)


Toggle_Select:

*
*

	bra	MessageLoop

* Datenbereich


BranchTabelle:
	dc.l	BranchTabMenu1
	dc.l	BranchTabMenu2

BranchTabMenu1
	dc.l	Exit,Toggle_Select

BranchTabMenu2
	dc.l	Toggle_Select,Toggle_Select

Class:		dc.l	0
Code:		dc.l	0
Message:	dc.l	0
UPort:		dc.l	0
IntBase:	dc.l	0
WindowHD:	dc.l	0

IntName:	dc.b	"intuition.library",0
	even

WinName:	dc.b	"Menu",0
	even

WindowArgs:	
	dc.w	0,0
	dc.w	640,256
	dc.b	1,3
	dc.l	$100
	dc.l	$1000
	dc.l	0
	dc.l	Image1
	dc.l	WinName
	dc.l	0
	dc.l	0
	dc.w	100,50
	dc.w	200,100
	dc.w	1


* Menu 1

Menu1:
	dc.l	Menu2
	dc.w	10,0
	dc.w	150,0
	dc.w	1
	dc.l	MenuTxt1
	dc.l	MenuItem1_1
	dc.w	0,0,0,0

MenuTxt1:
	dc.b	"Projekt",0
	even

* Menu 1 Item 1

MenuItem1_1:
	dc.l	MenuItem1_2
	dc.w	0,0
	dc.w	200,12
	dc.w	%01010110
	dc.l	0
	dc.l	MenuItemTxt1_1
	dc.l	0
	dc.b	"B"
	even
	dc.l	0
	dc.w	0

MenuItemTxt1_1:
	dc.b	0,1
	dc.b	0
	even
	dc.w	5,4
	dc.l	0
	dc.l	MITxt1_1
	dc.l	0

MITxt1_1:
	dc.b	"Beenden",0
	even

* Menu 1 Item 2

MenuItem1_2:
	dc.l	0
	dc.w	0,12
	dc.w	200,12
	dc.w	%01011111
	dc.l	0
	dc.l	MenuItemTxt1_2
	dc.l	0
	dc.b	"T"
	even
	dc.l	0
	dc.w	0

MenuItemTxt1_2:
	dc.b	0,2
	dc.b	0
	even
	dc.w	25,4
	dc.l	0
	dc.l	MITxt1_2
	dc.l	0

MITxt1_2:
	dc.b	"Toggle",0
	even



* Menu 2

Menu2:
	dc.l	0
	dc.w	200,0
	dc.w	150,0
	dc.w	1
	dc.l	MenuTxt2
	dc.l	MenuItem2_1
	dc.w	0,0,0,0

MenuTxt2:
	dc.b	"Projekt",0
	even

* Menu 2 Item 1

MenuItem2_1:
	dc.l	MenuItem2_2
	dc.w	10,5
	dc.w	110,25
	dc.w	%10011011
	dc.l	0
	dc.l	MenuItemTxt2_1
	dc.l	0
	dc.b	0
	even
	dc.l	0
	dc.w	0

MenuItemTxt2_1:
	dc.b	0,1
	dc.b	0
	even
	dc.w	10,8
	dc.l	0
	dc.l	MITxt2_1
	dc.l	0

MITxt2_1:
	dc.b	"Save as IFF",0
	even

* Menu 2 Item 2

MenuItem2_2:
	dc.l	0
	dc.w	130,0
	dc.w	50,34
	dc.w	%01011000
	dc.l	%1
	dc.l	Image
	dc.l	0
	dc.b	0
	even
	dc.l	0
	dc.w	0


* Image-Data

Image:
	dc.w	0,0
	dc.w	50,34
	dc.w	2
	dc.l	ImageData
	dc.b	%11,0
	dc.l	0

	Section	"",Data_C

ImageData:
 dc.l  $00000000,$00000000,$00000000,$00004000,$00000000,$00004000,$00000000,$09004000
 dc.l  $01FFFFFE,$0D804000,$013FFFF3,$0B404000,$013FFFFF,$09204000,$013FFFFF,$08904000
 dc.l  $013FFFF3,$38484000,$013FFFF3,$20244000,$013FFFF3,$20124000,$013FFFF3,$20244000
 dc.l  $013FFFF3,$38484000,$013FFFF3,$08904000,$013FFFF3,$09204000,$013FFFF3,$0B404000
 dc.l  $013FFFF3,$0D804000,$013FFFF3,$09204000,$01000003,$03604000,$01000003,$05A04000
 dc.l  $0100FFE3,$09204000,$0101FFF3,$12204000,$0101FF13,$24384000,$0101FF93,$48084000
 dc.l  $0101FF93,$90084000,$0101FF93,$48084000,$0101FF93,$24384000,$0101FF93,$12204000
 dc.l  $0081FFB3,$09204000,$007FFFDF,$05A04000,$003FFFEF,$03604000,$00000000,$01204000
 dc.l  $00000000,$00004000,$7FFFFFFF,$FFFFC000,$FFFFFFFF,$FFFFC000,$80000000,$00000000
 dc.l  $80000000,$00000000,$80000000,$00000000,$80000000,$00000000,$80DFFFEC,$04800000
 dc.l  $80DFFFE0,$06C00000,$80DFFFE0,$07600000,$80DFFFEC,$07B00000,$80DFFFEC,$1FD80000
 dc.l  $80DFFFEC,$1FEC0000,$80DFFFEC,$1FD80000,$80DFFFEC,$07B00000,$80DFFFEC,$07600000
 dc.l  $80DFFFEC,$06C00000,$80DFFFEC,$04800000,$80DFFFEC,$00000000,$80C0000C,$00000000
 dc.l  $80FFFFFC,$00000000,$80FFFFFC,$02400000,$80FF001C,$06C00000,$80FEFC0C,$0DC00000
 dc.l  $80FE8E0C,$1BC00000,$80FE8E0C,$37F00000,$80FE8E0C,$6FF00000,$80FE8E0C,$37F00000
 dc.l  $80FE8E0C,$1BC00000,$80FE8E0C,$0DC00000,$807EFE0C,$06C00000,$80000000,$02400000
 dc.l  $80000000,$00000000,$80000000,$00000000,$80000000,$00000000,$80000000,$00000000
 dc.l  $0AAA0000,$0FFF05AF




Image1:
	dc.w	0,-1
	dc.w	16,9
	dc.w	2
	dc.l	ImageData1
	dc.b	%11,0
	dc.l	0


ImageData1:

	dc.w	%1101101101101111
	dc.w	%1100100100100111
	dc.w	%1101001001001011
	dc.w	%1101101101101101
	dc.w	%1101101101101110
	dc.w	%1101101101101101
	dc.w	%1101001001001011
	dc.w	%1100100100100111
	dc.w	%1101101101101111

	dc.w	%1111111111111111
	dc.w	%1111111111111111
	dc.w	%1110110110110111
	dc.w	%1110010010010011
	dc.w	%1110010010010001
	dc.w	%1110010010010011
	dc.w	%1110110110110111
	dc.w	%1111111111111111
	dc.w	%1111111111111111

* Ende




*
* Kapitel 5
* Demonstrationsprogramm f�r die Image-Struktur
*

ExecBase		=	4
OldOpenLib		=	-408
CloseLib		=	-414
GetMsg			=	-372
ReplyMsg		=	-378
WaitPort		=	-384
OpenWindow		=	-204
CloseWindow		=	-72
DrawImage		=	-114

Start:
	move.l	ExecBase,a6
	lea	IntName,a1
	jsr	OldOpenLib(a6)
	move.l	d0,IntBase
	beq	IntError

	move.l	IntBase,a6
	lea	WindowArgs,a0
	jsr	OpenWindow(a6)
	move.l	d0,WindowHD
	beq	WinError

	move.l	WindowHD,a0
	move.l	86(a0),UPort
	move.l	50(a0),RPort

	move.l	RPort,a0		; RastPort auf dem das Image gezeichnet werden soll
	lea	Image,a1		; Zeiger auf Image-Struktur
	move.l	#40,d0			; X und
	move.l	#14,d1			; Y Koordinate
	jsr	DrawImage(a6)		; Zeichnen!

MessageLoop:
	move.l	ExecBase,a6

	move.l	UPort,a0
	jsr	GetMsg(a6)
	move.l	d0,Message
	bne	MessageBranch

	move.l	UPort,a0
	jsr	WaitPort(a6)
	bra	MessageLoop

Exit:
	move.l	IntBase,a6
	move.l	WindowHD,a0
	jsr	CloseWindow(a6)

WinError:
	move.l	ExecBase,a6
	move.l	IntBase,a1
	jsr	CloseLib(a6)

IntError:
	rts


MessageBranch:
	move.l	Message,a0
	move.l	20(a0),Class

	move.l	Message,a1
	jsr	ReplyMsg(a6)

	cmp.l	#$200,Class
	beq	Exit

	bra	MessageLoop

* Datenbereich

Class:		dc.l	0
Message:	dc.l	0
UPort:		dc.l	0
RPort:		dc.l	0
IntBase:	dc.l	0
WindowHD:	dc.l	0

IntName:	dc.b	"intuition.library",0	
	even

WinName:	dc.b	"< Close-Gadget !!",0
	even

WindowArgs:
	dc.w	200,60
	dc.w	240,40
	dc.b	1,3
	dc.l	$200
	dc.l	$1100F
	dc.l	0
	dc.l	0
	dc.l	WinName
ScreenHD
	dc.l	0
	dc.l	0
	dc.w	100,50
	dc.w	200,100
	dc.w	1


***************************************
* Image-Data

Image:
	dc.w	0,0
	dc.w	32,20
	dc.w	2
	dc.l	ImageData
	dc.b	%11,0
	dc.l	0

	Section	"",Data_C

ImageData:

	dc.l	%11111111111111111111111111111111
	dc.l	%10000000000000000000000000000000
	dc.l	%10000000000000000000000000001000
	dc.l	%10000000000000000000000000001000
	dc.l	%10000000000000000000000000001000
	dc.l	%10000000000000000000000000001000
	dc.l	%10000000001111111111110000001000
	dc.l	%10000000001111111111100000001000
	dc.l	%10000000001111111111100000001000
	dc.l	%10000000001111111111100000001000
	dc.l	%10000000001111111111100000001000
	dc.l	%10000000001111111111100000001000
	dc.l	%10000000001000000000000000001000
	dc.l	%10000000000000000000000000001000
	dc.l	%10000000000000000000000000001000
	dc.l	%10000000000000000000000000001000
	dc.l	%10000000000000000000000000001000
	dc.l	%10001111111111111111111111111000
	dc.l	%10000000000000000000000000000000
	dc.l	%10000000000000000000000000000000

	dc.l	%00000000000000000000000000000000
	dc.l	%00000000000000000000000000000001
	dc.l	%00011111111111111111111111111001
	dc.l	%00010000000000000000000000000001
	dc.l	%00010000000000000000000000000001
	dc.l	%00010000000000000000000000000001
	dc.l	%00010000000000000000000000000001
	dc.l	%00010000000111111111110000000001
	dc.l	%00010000000111111111110000000001
	dc.l	%00010000000111111111110000000001
	dc.l	%00010000000111111111110000000001
	dc.l	%00010000000111111111110000000001
	dc.l	%00010000000111111111110000000001
	dc.l	%00010000001111111111110000000001
	dc.l	%00010000000000000000000000000001
	dc.l	%00010000000000000000000000000001
	dc.l	%00010000000000000000000000000001
	dc.l	%00010000000000000000000000000001
	dc.l	%00000000000000000000000000000001
	dc.l	%11111111111111111111111111111111

* Ende


*
* Kapitel 12
* Demonstrationsprogramm f�r Fenster
*

ExecBase	=	4
OldOpenLib	=	-408
CloseLib	=	-414
GetMsg		=	-372
ReplyMsg	=	-378
WaitPort	=	-384
OpenWindow	=	-204
CloseWindow	=	-72
SetPointer	=	-270
MoveWindow	=	-168
SizeWindow	=	-288
WindowToBack	=	-306
WindowToFront	=	-312
ActivateWindow	=	-450

Start:
	move.l	ExecBase,a6
	lea	IntName,a1
	jsr	OldOpenLib(a6)
	move.l	d0,IntBase
	beq	IntError

	move.l	IntBase,a6
	lea	WindowArgs,a0
	jsr	OpenWindow(a6)
	move.l	d0,WindowHD
	beq	WinError

	move.l	WindowHD,a0
	lea	Mauszeiger,a1
	move.l	#10,d0
	move.l	#16,d1
	move.l	#-7,d2
	move.l	#-3,d3
	jsr	SetPointer(a6)


	lea	MoveTab,a5
Loop0:
	move.l	WindowHD,a0
	move.l	(a5)+,d0
	move.l	(a5)+,d1
	jsr	MoveWindow(a6)
	bsr	Wait
	cmp.l	#-1,(a5)
	bne	Loop0


	lea	MoveTab,a5
Loop1:
	move.l	WindowHD,a0
	move.l	(a5)+,d0
	move.l	(a5)+,d1
	jsr	SizeWindow(a6)
	bsr	Wait
	cmp.l	#-1,(a5)
	bne	Loop1

	move.l	WindowHD,a0
	jsr	WindowToBack(a6)

	bsr	Wait

	move.l	WindowHD,a0
	jsr	WindowToFront(a6)



	move.l	WindowHD,a0
	move.l	86(a0),UPort

MessageLoop:
	move.l	ExecBase,a6

	move.l	UPort,a0
	jsr	GetMsg(a6)
	move.l	d0,Message
	bne	MessageBranch

	move.l	UPort,a0
	jsr	WaitPort(a6)
	bra	MessageLoop

Exit:
	move.l	IntBase,a6
	move.l	WindowHD,a0
	jsr	CloseWindow(a6)

WinError:
	move.l	ExecBase,a6
	move.l	IntBase,a1
	jsr	CloseLib(a6)

IntError:
	rts


Wait:
	move.l	#$20000,d0
Wait0:	sub.l	#1,d0
	bne	Wait0
	rts


MessageBranch:
	move.l	Message,a0
	move.l	20(a0),Class

	move.l	Message,a1
	jsr	ReplyMsg(a6)

	cmp.l	#$200,Class
	beq	Exit

	cmp.l	#$80000,Class
	bne	MessageLoop

	move.l	IntBase,a6
	move.l	WindowHD,a0
	jsr	ActivateWindow(a6)
	bra	MessageLoop

* Datenbereich

Class:		dc.l	0
Message:	dc.l	0
UPort:		dc.l	0
IntBase:	dc.l	0
WindowHD:	dc.l	0

IntName:	dc.b	"intuition.library",0
	even

WinName:	dc.b	"Das Close-Gadget beendet das Programm !",0
	even

WindowArgs:
	dc.w	140,40
	dc.w	360,160
	dc.b	1,3
	dc.l	$80200
	dc.l	$1100F
	dc.l	0
	dc.l	0
	dc.l	WinName
ScreenHD:
	dc.l	0
	dc.l	0
	dc.w	100,50
	dc.w	200,100
	dc.w	1


MoveTab:
	dc.l	-140,-40
	dc.l	040,080
	dc.l	080,-40

	dc.l	-1


	Section	"",Data_C


Mauszeiger:
	dc.w	0,0	

 
	dc.w	%0000011000000000,%0000000000000000
	dc.w	%0000011000000000,%0000000100000000
	dc.w	%0000011000000000,%0000000100000000
	dc.w	%0000000000000000,%0000001100000000
	dc.w	%1111011011110000,%0000001100000000
	dc.w	%0000000000000000,%0011111111111000
	dc.w	%0000011000000000,%0000000100000000
	dc.w	%0000011000000000,%0000000100000000
	dc.w	%0000011000000000,%0000000100000000
	dc.w	%0000000000000000,%0000001100000000






	dc.w	0,0

* Ende


*
* Kapitel 5
* Demonstrationsprogramm f�r dir Funktionen BuildSysRequest und FreeSysRequest
*

ExecBase		=	4
GetMsg			=	-372
WaitPort		=	-384
ReplyMsg		=	-378
OldOpenLib		=	-408
CloseLib		=	-414
BuildSysRequest		=	-360
FreeSysRequest		=	-372

Start:
	move.l	ExecBase,a6
	lea	IntName,a1
	jsr	OldOpenLib(a6)
	move.l	d0,IntBase

Loop:
	move.l	IntBase,a6
	move.l	#0,a0
	lea	BodyText,a1
	lea	PosText,a2
	lea	NegText,a3
	move.l	#$10000,d0
	move.l	#10,d2
	move.l	#100,d3
	jsr	BuildSysRequest(a6)
	move.l	d0,WindowHD

	move.l	d0,a0
	move.l	86(a0),UPort

MessageLoop:
	move.l	ExecBase,a6

	move.l	UPort,a0
	jsr	GetMsg(a6)
	move.l	d0,Message
	bne	MessageBranch

	move.l	UPort,a0
	jsr	WaitPort(a6)
	bra	MessageLoop

Exit:	move.l	IntBase,a6
	move.l	WindowHD,a0
	jsr	FreeSysRequest(a6)	

	move.l	ExecBase,a6
	move.l	IntBase,a1
	jsr	CloseLib(a6)
	rts

MessageBranch:
	move.l	Message,a0		; Zeiger auf Message-Struktur nach a0
	move.l	20(a0),Class		; IDCMP-Kennung auslesen

	move.l	Message,a1		; Meldung best�tigen
	jsr	ReplyMsg(a6)

	cmp.l	#$10000,Class
	beq	Exit

	bra	MessageLoop


* Datenbereich

Class:		dc.l	0
Message:	dc.l	0
UPort:		dc.l	0
WindowHD:	dc.l	0

IntName:	dc.b	"intuition.library",0
	even
IntBase:	dc.l	0

BodyText:
	dc.b	0,0,0
	even
	dc.w	30,60
	dc.l	0,BodyData,0
BodyData:
	dc.b	"Wollen sie diesen AutoRequester beenden ???",0
	even

PosText:
	dc.b	0,0,0
	even
	dc.w	0,0
	dc.l	0,PosData,0
PosData:
	dc.b	"JA, weg mit dem Sch..",0
	even

NegText:
	dc.b	0,0,0
	even
	dc.w	0,0
	dc.l	0,NegData,0
NegData:
	dc.b	"Auf keinen Fall !",0
	even

* Ende

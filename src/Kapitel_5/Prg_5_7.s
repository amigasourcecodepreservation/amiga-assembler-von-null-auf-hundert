*
* Kapitel 5
* Demonstrationsprogramm f�r die Border-Struktur
*

ExecBase		=	4
OldOpenLib		=	-408
CloseLib		=	-414
GetMsg			=	-372
ReplyMsg		=	-378
WaitPort		=	-384
OpenWindow		=	-204
CloseWindow		=	-72
DrawBorder		=	-108

Start:
	move.l	ExecBase,a6
	lea	IntName,a1
	jsr	OldOpenLib(a6)		; Int-Lib. offnen
	move.l	d0,IntBase
	beq	IntError

	move.l	IntBase,a6
	lea	WindowArgs,a0
	jsr	OpenWindow(a6)		; Window �ffnen
	move.l	d0,WindowHD
	beq	WinError

	move.l	WindowHD,a0
	move.l	86(a0),UPort		; User-Port auslesen
	move.l	50(a0),RPort		; Rast-Port auslesen

	move.l	RPort,a0
	lea	Border,a1
	move.l	#415,d0
	move.l	#42,d1
	jsr	DrawBorder(a6)		; Border zeichnen

MessageLoop:
	move.l	ExecBase,a6

	move.l	UPort,a0
	jsr	GetMsg(a6)		; Nachrichten abholen
	move.l	d0,Message
	bne	MessageBranch

	move.l	UPort,a0
	jsr	WaitPort(a6)		; warten
	bra	MessageLoop

Exit:
	move.l	IntBase,a6
	move.l	WindowHD,a0
	jsr	CloseWindow(a6)		; Fenster schlie�en

WinError:
	move.l	ExecBase,a6
	move.l	IntBase,a1
	jsr	CloseLib(a6)		; Int-Lib schlie�en

IntError:
	rts


MessageBranch:
	move.l	Message,a0
	move.l	20(a0),Class

	move.l	Message,a1
	jsr	ReplyMsg(a6)		; Nachricht best�tigen

	cmp.l	#$200,Class		; 
	beq	Exit

	bra	MessageLoop

* Datenbereich

Class:		dc.l	0
Message:	dc.l	0
UPort:		dc.l	0
RPort:		dc.l	0
IntBase:	dc.l	0
WindowHD:	dc.l	0

IntName:	dc.b	"intuition.library",0	
	even

WinName:	dc.b	"Das Close-Gadget beendet das Programm !",0
	even

WindowArgs:
	dc.w	0,10
	dc.w	640,100
	dc.b	1,3
	dc.l	$200
	dc.l	$1100F
	dc.l	0
	dc.l	0
	dc.l	WinName
ScreenHD
	dc.l	0
	dc.l	0
	dc.w	100,50
	dc.w	200,100
	dc.w	1


***************************************
* Border-Data

Border:
	dc.w	-210,0
	dc.b	2,0
	dc.b	0,5
	dc.l	BorderData
	dc.l	Border0

BorderData:
	dc.w	0,0
	dc.w	200,0
	dc.w	200,20
	dc.w	0,20
	dc.w	0,0

Border0:
	dc.w	-210,0
	dc.b	1,0,0,3
	dc.l	BorderData0,Border1

BorderData0:
	dc.w	201,1,201,21,2,21

Border1:
	dc.w	0,0
	dc.b	2,0,0,3
	dc.l	BorderData1,Border2

BorderData1:
	dc.w	0,20,0,0,40,0

Border2:
	dc.w	0,0
	dc.b	1,0,0,3
	dc.l	BorderData2,Border3

BorderData2:
	dc.w	40,0,40,20,0,20

Border3:
	dc.w	50,0
	dc.b	1,0,0,3
	dc.l	BorderData3,Border4

BorderData3:
	dc.w	0,20,0,0,40,0

Border4:
	dc.w	50,0
	dc.b	2,0,0,3
	dc.l	BorderData4,0

BorderData4:
	dc.w	40,0,40,20,0,20

* Ende

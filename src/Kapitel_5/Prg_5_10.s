*
* Kapitel 5
* Demonstrationsprogramm f�r Gadgets
*

UPort_Offset	=	86
Class_Offset	=	20
GadAddrOffset	=	28
GadgetDown	=	$40
GadNumber	=	38
WinClose	=	$200

ExecBase	=	4
OldOpenLib	=	-408
CloseLib	=	-414
GetMsg		=	-372
ReplyMsg	=	-378
WaitPort	=	-384

OpenWindow	=	-204
CloseWindow	=	-72
WindowToBack	=	-306

Start:
	move.l	ExecBase,a6
	lea	IntName,a1
	jsr	OldOpenLib(a6)		; Int-Lib �ffnen
	move.l	d0,IntBase
	beq	IntError

	move.l	IntBase,a6
	lea	WindowArgs,a0
	jsr	OpenWindow(a6)		; Window �ffnen
	move.l	d0,WindowHD
	beq	WinError

	move.l	WindowHD,a0
	move.l	UPort_Offset(a0),UPort	; User-Port holen

MessageLoop:
	move.l	ExecBase,a6

	move.l	UPort,a0
	jsr	GetMsg(a6)		; Nachricht abholen
	move.l	d0,Message
	bne	MessageBranch

	move.l	UPort,a0
	jsr	WaitPort(a6)		; warten
	bra	MessageLoop

Exit:
	move.l	IntBase,a6
	move.l	WindowHD,a0
	jsr	CloseWindow(a6)		; Fenster schlie�en

WinError:
	move.l	ExecBase,a6
	move.l	IntBase,a1
	jsr	CloseLib(a6)		; Int-Lib. schlie�en

IntError:
	rts


MessageBranch:
	move.l	Message,a0		; Nachricht auswerten
	move.l	Class_Offset(a0),Class
	move.l	GadAddrOffset(a0),a0
	move.w	GadNumber(a0),GadID

	move.l	Message,a1		; und beantworten
	jsr	ReplyMsg(a6)

	cmp.l	#GadgetDown,Class	; Gadget bet�tigt
	beq	GadgetBranch

	bra	MessageLoop


GadgetBranch:

	cmp.w	#1,GadID		; GadID = 1?
	beq	Exit			; JA, dann "Exit"!

	bra	MessageLoop


* Datenbereich

GadID:		dc.l	0
Class:		dc.l	0
Message:	dc.l	0
UPort:		dc.l	0
IntBase:	dc.l	0
WindowHD:	dc.l	0

IntName:	dc.b	"intuition.library",0
	even

WinName:	dc.b	"� Close-Gadget !",0
	even

WindowArgs:
	dc.w	0,0,640,256
	dc.b	1,3
	dc.l	$60,$11000,Gadget0
	dc.l	0,0,0,0
	dc.w	0,0,0,0,1

********************************
* Boolean-Gadget-Daten

Gadget0:
	dc.l	Gadget1
	dc.w	30,15
	dc.w	16,10
	dc.w	4
	dc.w	1
	dc.w	1
	dc.l	Image
	dc.l	0
	dc.l	IText0
	dc.l	0
	dc.l	0
	dc.w	1
	dc.l	0


* IntuiText f�r das Gadget

IText0:
	dc.b	1,0,1,0
	dc.w	30,2
	dc.l	0,ITextData0,0
ITextData0:
	dc.b	"= Exit",0
	even

* Image Daten f�r das Boolean-Gadget

Image:
	dc.w	0,0,16,10,2
	dc.l	ImageData
	dc.b	%11,0
	dc.l	0

	Section	"",Data_C

ImageData:
	dc.w	%0000000000000000,%0000000000000001
	dc.w	%0000000000000001,%0000011111000001
	dc.w	%0000011110000001,%0000011110000001
	dc.w	%0000010000000001,%0000000000000001
	dc.w	%0000000000000001,%1111111111111111

	dc.w	%1111111111111111,%1000000000000000
	dc.w	%1000000000000000,%1000000000000000
	dc.w	%1000001111000000,%1000001111000000
	dc.w	%1000001111000000,%1000000000000000
	dc.w	%1000000000000000,%1000000000000000

********************************
* String-Gadget-Struktur

Gadget1:
	dc.l	Gadget2
	dc.w	300,17
	dc.w	200,10
	dc.w	0
	dc.w	1
	dc.w	4
	dc.l	Border1
	dc.l	0
	dc.l	IText1
	dc.l	0
	dc.l	StringInfo1
	dc.w	2
	dc.l	0

StringInfo1:
	dc.l	Buffer
	dc.l	UNDOBuffer
	dc.w	4
	dc.w	60
	dc.w	0
	dc.w	0
	dc.w	0
	dc.w	0
	dc.w	0
	dc.w	0
	dc.l	0
	dc.l	0
	dc.l	0

Buffer:	dc.b	"DH3:",0
	ds.b	55

UNDOBuffer:
	ds.b	60

* IntuiText f�r das Gadget

IText1:
	dc.b	1,0,1,0
	dc.w	-82,0
	dc.l	0,ITextData1,0
ITextData1:
	dc.b	"Filename:",0
	even

* Border f�r das Gadget

Border1:
	dc.w	-2,-2
	dc.b	3,2,0,5
	dc.l	BData1,0

BData1:	dc.w	0,0,203,0,203,11,0,11,0,0

* Ende


********************************
* Proportional-Gadget-Struktur

Gadget2:
	dc.l	0
	dc.w	20,40
	dc.w	600,200
	dc.w	0
	dc.w	1
	dc.w	3
	dc.l	Image2
	dc.l	0
	dc.l	0
	dc.l	0
	dc.l	PropInfo
	dc.w	3
	dc.l	0

PropInfo:
	dc.w	7
	dc.w	$8000
	dc.w	$8000
	dc.w	$8000/4
	dc.w	$8000/4
	dc.w	0
	dc.w	0
	dc.w	0
	dc.w	0
	dc.w	0
	dc.w	0

Image2:	ds.l	100

* IntuiText f�r das Gadget

IText2:
	dc.b	1,0,1,0
	dc.w	-82,0
	dc.l	0,ITextData2,0
ITextData2:
	dc.b	"Filename:",0
	even

* Border f�r das Gadget

Border2:
	dc.w	-2,-2
	dc.b	3,2,0,5
	dc.l	BData2,0

BData2:
	dc.w	0,0,203,0,203,11,0,11,0,0

* Ende



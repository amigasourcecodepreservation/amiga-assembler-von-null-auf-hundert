*
* Kapitel 5
* Demonstrationsprogramm f�r die IntuiText-Struktur
*

ExecBase		=	4
OldOpenLib		=	-408
CloseLib		=	-414
GetMsg			=	-372
ReplyMsg		=	-378
WaitPort		=	-384
OpenWindow		=	-204
CloseWindow		=	-72
PrintIText		=	-216

Start:
	move.l	ExecBase,a6
	lea	IntName,a1
	jsr	OldOpenLib(a6)
	move.l	d0,IntBase
	beq	IntError

	move.l	IntBase,a6
	lea	WindowArgs,a0
	jsr	OpenWindow(a6)
	move.l	d0,WindowHD
	beq	WinError

	move.l	WindowHD,a0
	move.l	86(a0),UPort
	move.l	50(a0),RPort

	move.l	RPort,a0
	lea	IText0,a1
	move.l	#10,d0
	move.l	#10,d1
	jsr	PrintIText(a6)

MessageLoop:
	move.l	ExecBase,a6

	move.l	UPort,a0
	jsr	GetMsg(a6)
	move.l	d0,Message
	bne	MessageBranch

	move.l	UPort,a0
	jsr	WaitPort(a6)
	bra	MessageLoop

Exit:
	move.l	IntBase,a6
	move.l	WindowHD,a0
	jsr	CloseWindow(a6)

WinError:
	move.l	ExecBase,a6
	move.l	IntBase,a1
	jsr	CloseLib(a6)

IntError:
	rts


MessageBranch:
	move.l	Message,a0
	move.l	20(a0),Class

	move.l	Message,a1
	jsr	ReplyMsg(a6)

	cmp.l	#$200,Class
	beq	Exit

	bra	MessageLoop

* Datenbereich

Class:		dc.l	0
Message:	dc.l	0
UPort:		dc.l	0
RPort:		dc.l	0
IntBase:	dc.l	0
WindowHD:	dc.l	0

IntName:	dc.b	"intuition.library",0	
	even

WinName:	dc.b	"Das Close-Gadget beendet das Programm !",0
	even

WindowArgs:
	dc.w	50,40
	dc.w	540,40
	dc.b	1,3
	dc.l	$200
	dc.l	$1100F
	dc.l	0
	dc.l	0
	dc.l	WinName
ScreenHD
	dc.l	0
	dc.l	0
	dc.w	100,50
	dc.w	200,100
	dc.w	1


* IntuiText-Struktur


IText0:
	dc.b	1,3
	dc.b	0
	even
	dc.w	10,10
	dc.l	0
	dc.l	ITextData0
	dc.l	IText1

ITextData0:
	dc.b	"IntuiText-Demo    :    Alles was ich will ist ein Computer !!!",0
	even

IText1:
	dc.b	2,3,0
	even
	dc.w	11,9
	dc.l	0,ITextData0,0

* Ende


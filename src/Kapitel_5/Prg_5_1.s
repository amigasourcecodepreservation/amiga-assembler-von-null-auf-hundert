*
* Kapitel 5
* Demonstrationsprogramm f�r Windows
*

ExecBase		=	4
CloseLib		=	-414
OldOpenLib		=	-408
OpenWindow		=	-204
CloseWindow		=	-72

Start:
	move.l	ExecBase,a6	; Basis der Exec-Library in a6 laden
	lea	IntName,a1	; Zeiger auf den Namen der zu �ffneden Library
	jsr	OldOpenLib(a6)	; => Exec
	move.l	d0,IntBase	; Basisadresse speichern
	beq	IntError	; Fehler aufgetreten ?

	move.l	IntBase,a6	; Basis der Intuition-Library laden
	lea	WindowArgs,a0	; Zeiger auf Windowdefinitionen in a0 �bergeben
	jsr	OpenWindow(a6)	; => Intuition
	move.l	d0,WindowHD	; Zeiger auf die Window-Struktur speichern
	beq	WinError	; Fehler aufgetreten ?

WaitLMT:
	btst	#6,$bfe001	; linke Maustaste gedr�ckt ?
	bne	WaitLMT		; nein dann warten

	move.l	IntBase,a6	; Aufruf einer Int.-Funktion vorbereiten
	move.l	WindowHD,a0	; Zeiger auf das Fenster �bergeben
	jsr	CloseWindow(a6)	; => Intuition

WinError:
	move.l	ExecBase,a6	; Basisadresse der Exec-Library nach a6
	move.l	IntBase,a1	; Zeiger der zu schlie�enden Library
	jsr	CloseLib(a6)	; => Exec

IntError:
	rts			; ReTurn from Subroutine

* Datenbereich

IntBase:	dc.l	0	; Variable f�r die Basisadresse der Int-Lib
WindowHD:	dc.l	0	; Speicher f�r den Zeiger auf die Window-Struktur

IntName:	dc.b	"intuition.library",0	; Name der Library die ge�ffnet werden soll
	even					; PC auf geraden Wert bringen

WinName:	dc.b	"Um das Fenster zu schlie�en bitte LMT dr�cken !!",0
	even					; Titel des Fensters und PC auf Wortgrenze setzen

WindowArgs:			; Die NewWindow-Struktur
	dc.w	90,10		; Position des Fensters
	dc.w	460,200		; Breite und H�he
	dc.b	1,3		; Farbe f�r Hinter- und Vordergrund
	dc.l	$600		; IDCMP-Flags
	dc.l	$1100F		; Window-Flags
	dc.l	0		; Zeiger auf erstes Gadget
	dc.l	0		; Zeiger auf CheckMark Grafik
	dc.l	WinName		; Zeiger auf Titelzeichenkette
ScreenHD
	dc.l	0		; Zeiger auf eine Screen-Struktur in dem das Fenster erscheinen soll
	dc.l	0		; Platz f�r Adresse einer Eigenen BitMap
	dc.w	100,50		; Minimalwerte der Fenstergr��e
	dc.w	200,100		; Maximalwerte der Fenstergr��e
	dc.w	1		; Typ des Fensters

* Ende


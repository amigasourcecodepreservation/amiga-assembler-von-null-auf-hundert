*
* Kapitel 5
* Demonstratiosprogramm f�r DisplayAlerts
*

ExecBase	=	4
OldOpenLib	=	-408
CloseLib	=	-414
DisplayAlert	=	-90

Start:
	move.l	ExecBase,a6

	lea	IntName,a1
	jsr	OldOpenLib(a6)		; Int-Lib �ffnen
	move.l	d0,IntBase

Loop:
	move.l	IntBase,a6
	lea	AlertData,a0
	move.l	#0,d0
	move.l	#55,d1
	jsr	DisplayAlert(a6)	; Alert erzeugen

	tst.l	d0
	beq	Loop

	move.l	ExecBase,a6
	move.l	IntBase,a1
	jsr	CloseLib(a6)		; Int-Lib schlie�en
	rts

* Datenbereich

IntName:	dc.b	"intuition.library",0
	even
IntBase:	dc.l	0

AlertData:
	dc.w	240
	dc.b	15
	dc.b	"!!!! ACHTUNG !!!!",0
	dc.b	-1

	dc.w	170
	dc.b	25
	dc.b	"Dies ist ein RECOVERY-Display-Alert",0
	dc.b	-1

	dc.w	180
	dc.b	45
	dc.b	"LMT - Ende         RMT - Schleife",0
	dc.b	0
	even

* Ende

*
* Kapitel 5
* Demonstrationsprogramm f�r Screens
*

ExecBase		=	4
OldOpenLib		=	-408
CloseLib		=	-414
GetMsg			=	-372
ReplyMsg		=	-378
WaitPort		=	-384
OpenScreen		=	-198
CloseScreen		=	-66
OpenWindow		=	-204
CloseWindow		=	-72

Start:
	move.l	ExecBase,a6
	lea	IntName,a1
	jsr	OldOpenLib(a6)
	move.l	d0,IntBase
	beq	IntError

	move.l	IntBase,a6
	lea	ScreenArgs,a0
	jsr	OpenScreen(a6)
	move.l	d0,ScreenHD
	beq	ScrError

	lea	WindowArgs,a0
	jsr	OpenWindow(a6)
	move.l	d0,WindowHD
	beq	WinError

	move.l	WindowHD,a0
	move.l	86(a0),UPort

MessageLoop:
	move.l	ExecBase,a6

	move.l	UPort,a0
	jsr	GetMsg(a6)
	move.l	d0,Message
	bne	MessageBranch

	move.l	UPort,a0
	jsr	WaitPort(a6)
	bra	MessageLoop

Exit:
	move.l	IntBase,a6
	move.l	WindowHD,a0
	jsr	CloseWindow(a6)

WinError:
	move.l	ScreenHD,a0
	jsr	CloseScreen(a6)

ScrError:
	move.l	ExecBase,a6
	move.l	IntBase,a1
	jsr	CloseLib(a6)

IntError:
	rts


MessageBranch:
	move.l	Message,a0
	move.l	20(a0),Class

	move.l	Message,a1
	jsr	ReplyMsg(a6)

	cmp.l	#$200,Class
	beq	Exit

	bra	MessageLoop

* Datenbereich

Class:		dc.l	0
Message:	dc.l	0
UPort:		dc.l	0
IntBase:	dc.l	0
WindowHD:	dc.l	0

IntName:	dc.b	"intuition.library",0
	even

WinName:	dc.b	"Das Close-Gadget beendet das Programm !",0
	even

ScrName:	dc.b	"CUSTOMSCREEN",0
	even

WindowArgs:
	dc.w	90,10
	dc.w	460,200
	dc.b	1,3
	dc.l	$200
	dc.l	$1100F
	dc.l	0
	dc.l	0
	dc.l	WinName
ScreenHD
	dc.l	0
	dc.l	0
	dc.w	100,50
	dc.w	200,100
	dc.w	15

ScreenArgs:
	dc.w	0,0
	dc.w	640,256
	dc.w	2
	dc.b	2,1
	dc.w	$8000
	dc.w	15
	dc.l	0
	dc.l	ScrName
	dc.l	0
	dc.l	0

* Ende

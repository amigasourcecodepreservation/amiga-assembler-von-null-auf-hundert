*
* Kapitel 5
* Demonstrationsprogramm f�r AutoRequester
*

ExecBase		=	4
OldOpenLib		=	-408
CloseLib		=	-414
AutoRequest		=	-348

Start:
	move.l	ExecBase,a6
	lea	IntName,a1
	jsr	OldOpenLib(a6)		; Int-Lib. �ffnen
	move.l	d0,IntBase

Loop:
	move.l	IntBase,a6
	move.l	#0,a0
	lea	BodyText,a1
	lea	PosText,a2
	lea	NegText,a3
	move.l	#$10000,d0
	move.l	#$8000,d1
	move.l	#10,d2
	move.l	#100,d3
	jsr	AutoRequest(a6)		; Requester erzeugen

	tst.l	d0
	beq	Loop

	move.l	ExecBase,a6
	move.l	IntBase,a1
	jsr	CloseLib(a6)		; Int-Lib. schlie�en
	rts

* Datenbereich

IntName:	dc.b	"intuition.library",0
	even
IntBase:	dc.l	0

BodyText:
	dc.b	0,0,0
	even
	dc.w	30,60
	dc.l	0,BodyData,0
BodyData:
	dc.b	"Wollen sie diesen AutoRequester beenden ???",0
	even

PosText:
	dc.b	0,0,0
	even
	dc.w	0,0
	dc.l	0,PosData,0
PosData:
	dc.b	"JA, weg mit dem Sch..",0
	even

NegText:
	dc.b	0,0,0
	even
	dc.w	0,0
	dc.l	0,NegData,0
NegData:
	dc.b	"Auf keinen Fall !",0
	even

* Ende

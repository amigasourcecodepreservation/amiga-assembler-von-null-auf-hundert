*
* Kapitel 5
* Demonstrationsprogramm f�r Screens
*

ExecBase		=	4
OldOpenLib		=	-408
CloseLib		=	-414
GetMsg			=	-372
ReplyMsg		=	-378
WaitPort		=	-384
OpenScreen		=	-198
CloseScreen		=	-66
OpenWindow		=	-204
CloseWindow		=	-72
DisplayBeep		=	-96
WBenchToBack		=	-336
WBenchToFront		=	-342
MoveScreen		=	-162

Start:
	move.l	ExecBase,a6
	lea	IntName,a1
	jsr	OldOpenLib(a6)
	move.l	d0,IntBase
	beq	IntError

	move.l	IntBase,a6
	lea	ScreenArgs,a0
	jsr	OpenScreen(a6)
	move.l	d0,ScreenHD
	beq	ScrError

	lea	WindowArgs,a0
	jsr	OpenWindow(a6)
	move.l	d0,WindowHD
	beq	WinError

	move.l	WindowHD,a0
	move.l	86(a0),UPort

MessageLoop:
	move.l	ExecBase,a6

	move.l	UPort,a0
	jsr	GetMsg(a6)
	move.l	d0,Message
	bne	MessageBranch

	move.l	UPort,a0
	jsr	WaitPort(a6)
	bra	MessageLoop

Exit:
	move.l	#256/5,d7
	move.l	IntBase,a6
MoveLoop:
	move.l	ScreenHD,a0
	move.l	#0,d0
	move.l	#5,d1
	jsr	MoveScreen(a6)
	dbra	d7,MoveLoop

	move.l	IntBase,a6
	move.l	WindowHD,a0
	jsr	CloseWindow(a6)

WinError:
	move.l	ScreenHD,a0
	jsr	CloseScreen(a6)

ScrError:
	move.l	ExecBase,a6
	move.l	IntBase,a1
	jsr	CloseLib(a6)

IntError:
	rts


MessageBranch:
	move.l	Message,a0
	move.l	20(a0),Class

	move.l	Message,a1
	jsr	ReplyMsg(a6)

	cmp.l	#$200,Class
	beq	Exit

	cmp.l	#$8000,Class
	beq	DiskIN

	cmp.l	#$10000,Class
	beq	DiskOUT

	cmp.l	#$80000,Class
	beq	WindowINACTIVE

	bra	MessageLoop



WindowINACTIVE:
	move.l	IntBase,a6
	move.l	ScreenHD,a0
	jsr	DisplayBeep(a6)
	bra	MessageLoop

DiskIN:
	move.l	IntBase,a6
	jsr	WBenchToBack(a6)
	bra	MessageLoop

DiskOUT:	
	move.l	IntBase,a6
	jsr	WBenchToFront(a6)
	bra	MessageLoop


* Datenbereich

Class:		dc.l	0
Message:	dc.l	0
UPort:		dc.l	0
IntBase:	dc.l	0
WindowHD:	dc.l	0

IntName:	dc.b	"intuition.library",0
	even

WinName:	dc.b	"Das Close-Gadget beendet das Programm !",0
	even

ScrName:	dc.b	"CUSTOMSCREEN",0
	even

WindowArgs:
	dc.w	90,10
	dc.w	460,200
	dc.b	1,3
	dc.l	$98200
	dc.l	$1100F
	dc.l	0
	dc.l	0
	dc.l	WinName
ScreenHD
	dc.l	0
	dc.l	0
	dc.w	100,50
	dc.w	200,100
	dc.w	15

ScreenArgs:
	dc.w	0,0
	dc.w	640,256
	dc.w	2
	dc.b	2,1
	dc.w	$8000
	dc.w	15
	dc.l	0
	dc.l	ScrName
	dc.l	0
	dc.l	0

* Ende

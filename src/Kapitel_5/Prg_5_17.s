*
* Kapitel 5
* Demonstrationsporgramm f�r DM-Requester
*

ExecBase		=	4
OldOpenLib		=	-408
CloseLib		=	-414
ReplyMsg		=	-378
WaitPort		=	-384
GetMsg			=	-372
OpenWindow		=	-204
CloseWindow		=	-72
InitRequest		=	-138
SetDMRequest		=	-258

ClearDMRequest		=	-48

Start:
	move.l	ExecBase,a6

	lea	IntName,a1
	jsr	OldOpenLib(a6)		; Int-Lib. �ffnen
	move.l	d0,IntBase
	beq	IntError

	move.l	IntBase,a6
	lea	WindowArgs,a0
	jsr	OpenWindow(a6)		; Fenster �ffnen
	move.l	d0,WindowHD
	beq	WinError

	move.l	WindowHD,a0
	move.l	86(a0),UPort		; User-Port holen

	lea	Requester,a0
	jsr	InitRequest(a6)		; Request-Struktur l�schen

	move.w	#-275,rq_RelLeft	; und mit Werten f�llen
	move.w	#-38,rq_RelTop

	move.w	#38*8,rq_Width
	move.w	#54,rq_Height
	move.l	#EndGadget,rq_ReqGadget
	move.l	#IText0,rq_ReqText
	move.l	#Border0,rq_ReqBorder
	move.w	#1,rq_Flags

	move.l	IntBase,a6
	lea	Requester,a1
	move.l	WindowHD,a0
	jsr	SetDMRequest(a6)	; DM-Requester installieren

MessageLoop:
	move.l	ExecBase,a6

	move.l	UPort,a0
	jsr	GetMsg(a6)		; Nachricht abholen
	move.l	d0,Message
	bne	MessageBranch

	move.l	UPort,a0
	jsr	WaitPort(a6)		; warten
	bra	MessageLoop
Exit:
	move.l	IntBase,a6
	move.l	WindowHD,a0
	jsr	ClearDMRequest(a6)	; DM-Requester l�schen

	move.l	IntBase,a6
	move.l	WindowHD,a0
	jsr	CloseWindow(a6)		; Window schlie�en

WinError:
	move.l	ExecBase,a6
	move.l	IntBase,a1
	jsr	CloseLib(a6)		; Int-Lib schlie�en
IntError:
	rts


MessageBranch:
	move.l	Message,a0		; Nachricht auswerten
	move.l	20(a0),Class
	move.l	28(a0),a0
	move.w	38(a0),GadID

	cmp.l	#$800,Class		; DM-Request
	bne	NoReq
	bset	#1,Flag			; Flag 1 setzen

NoReq:	move.l	Message,a1
	jsr	ReplyMsg(a6)		; Nachricht best�tigen

	btst	#1,Flag			; Flag gesetzt ?
	bne	ReqMessage		; Dann Behandlungsroutine des Requesters anspringen

	cmp.l	#$200,Class		; Fenster schlie�en?
	beq	Exit			; JA!

	bra	MessageLoop


ReqMessage:
	cmp.l	#$1000,Class		; EndRequest
	bne	OhNo
	bclr	#1,Flag			; Flag l�schen

OhNo:	bra	MessageLoop

* Datenbereich

GadID:		dc.w	0
Class:		dc.l	0
Flag:		dc.w	0
IntBase:	dc.l	0
WindowHD:	dc.l	0
Message:	dc.l	0
UPort:		dc.l	0

IntName:	dc.b	"intuition.library",0
	even
WinName:	dc.b	"< CloseGadget beendet das Programm!",0
	even

WindowArgs:
	dc.w	0,0,640,256
	dc.b	1,3
	dc.l	$1A00,$0100F,0,0,WinName,0,0
	dc.w	100,50,200,100,1

Requester:	dc.l	0
rq_LeftEdge:	dc.w	0
rq_TopEdge:	dc.w	0
rq_Width:	dc.w	0
rq_Height:	dc.w	0
rq_RelLeft	dc.w	0
rq_RelTop	dc.w	0
rq_ReqGadget:	dc.l	EndGadget
rq_ReqBorder:	dc.l	0
rq_ReqText:	dc.l	0
rq_Flags:	dc.w	0
rq_BackFill:	dc.b	0
		even
		dc.l	0
		ds.b	32
rq_ImageBitMap:	dc.l	0
rq_RWindow:	dc.l	0
		ds.b	36

*************************

EndGadget:
	dc.l	0
	dc.w	260
	dc.w	28
	dc.w	32
	dc.w	20
	dc.w	4
	dc.w	$05
	dc.w	1
	dc.l	Image0
	dc.l	0
	dc.l	0
	dc.l	0
	dc.l	0
	dc.w	1
	dc.l	0

************************


* IntuiText-Struktur


IText0:
	dc.b	1,3
	dc.b	0
	even
	dc.w	21,11
	dc.l	0
	dc.l	ITextData0
	dc.l	IText1

ITextData0:
	dc.b	"Dies ist ein DM-Requester !",0
	even

IText1:
	dc.b	2,3,0
	even
	dc.w	20,10
	dc.l	0,ITextData0,0


***************************************
* Border-Data

Border0:
	dc.w	0,0
	dc.b	2,0,0,3
	dc.l	BorderData0
	dc.l	Border1

BorderData0:
	dc.w	0,52
	dc.w	0,0
	dc.w	299,0

Border1:
	dc.w	0,0
	dc.b	1,0,0,3
	dc.l	BorderData1,0

BorderData1:
	dc.w	299,1,299,52,2,52

Border2:
	dc.w	0,0
	dc.b	2,0,0,3
	dc.l	BorderData2,0 *Border3

BorderData2:
	dc.w	0,40,0,0,40,0

Border3:
	dc.w	0,0
	dc.b	1,0,0,3
	dc.l	BorderData3,Border4

BorderData3:
	dc.w	40,0,40,20,0,20

Border4:
	dc.w	50,0
	dc.b	1,0,0,3
	dc.l	BorderData4,Border5

BorderData4:
	dc.w	0,20,0,0,40,0

Border5:
	dc.w	50,0
	dc.b	2,0,0,3
	dc.l	BorderData5,0

BorderData5:
	dc.w	40,0,40,20,0,20


***************************************
* Image-Data

Image0:
	dc.w	0,0
	dc.w	32,20
	dc.w	2
	dc.l	ImageData0
	dc.b	%11,0
	dc.l	0

	Section	"",Data_C

ImageData0:
	dc.l	%00000000000000000000000000000000
	dc.l	%00000000000000000000000000000001
	dc.l	%00011111111111111111111111111001
	dc.l	%00010000000000000000000000000001
	dc.l	%00010000000000000000000000000001
	dc.l	%00010000000000000000000000000001
	dc.l	%00010000000000000000000000000001
	dc.l	%00010000000111111111110000000001
	dc.l	%00010000000111111111110000000001
	dc.l	%00010000000111111111110000000001
	dc.l	%00010000000111111111110000000001
	dc.l	%00010000000111111111110000000001
	dc.l	%00010000000111111111110000000001
	dc.l	%00010000001111111111110000000001
	dc.l	%00010000000000000000000000000001
	dc.l	%00010000000000000000000000000001
	dc.l	%00010000000000000000000000000001
	dc.l	%00010000000000000000000000000001
	dc.l	%00000000000000000000000000000001
	dc.l	%11111111111111111111111111111111

	dc.l	%11111111111111111111111111111111
	dc.l	%10000000000000000000000000000000
	dc.l	%10000000000000000000000000001000
	dc.l	%10000000000000000000000000001000
	dc.l	%10000000000000000000000000001000
	dc.l	%10000000000000000000000000001000
	dc.l	%10000000001111111111110000001000
	dc.l	%10000000001111111111100000001000
	dc.l	%10000000001111111111100000001000
	dc.l	%10000000001111111111100000001000
	dc.l	%10000000001111111111100000001000
	dc.l	%10000000001111111111100000001000
	dc.l	%10000000001000000000000000001000
	dc.l	%10000000000000000000000000001000
	dc.l	%10000000000000000000000000001000
	dc.l	%10000000000000000000000000001000
	dc.l	%10000000000000000000000000001000
	dc.l	%10001111111111111111111111111000
	dc.l	%10000000000000000000000000000000
	dc.l	%10000000000000000000000000000000


* Ende



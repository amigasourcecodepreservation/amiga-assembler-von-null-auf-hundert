*
* Kapitel 5
* Demonstrationsprogramm f�r die Grafik-Strukturen
*

ExecBase		=	4
OldOpenLib		=	-408
CloseLib		=	-414
GetMsg			=	-372
ReplyMsg		=	-378
WaitPort		=	-384
OpenWindow		=	-204
CloseWindow		=	-72
PrintIText		=	-216
DrawBorder		=	-108
DrawImage		=	-114

Start:
	move.l	ExecBase,a6
	lea	IntName,a1
	jsr	OldOpenLib(a6)		; Lib �ffnen
	move.l	d0,IntBase
	beq	IntError

	move.l	IntBase,a6
	lea	WindowArgs,a0
	jsr	OpenWindow(a6)		; Win �ffnen
	move.l	d0,WindowHD
	beq	WinError

	move.l	WindowHD,a0
	move.l	86(a0),UPort
	move.l	50(a0),RPort

	move.l	RPort,a0
	lea	IText0,a1
	move.l	#30,d0
	move.l	#42,d1
	jsr	PrintIText(a6)		; IntuitText ausgeben

	move.l	RPort,a0
	lea	Border,a1
	move.l	#20,d0
	move.l	#30,d1
	jsr	DrawBorder(a6)		; Border zeichnen

	move.l	RPort,a0
	lea	Image,a1
	move.l	#230,d0
	move.l	#36,d1
	jsr	DrawImage(a6)		; Image zeichnen

MessageLoop:
	move.l	ExecBase,a6

	move.l	UPort,a0
	jsr	GetMsg(a6)		; Nachricht abholen
	move.l	d0,Message
	bne	MessageBranch

	move.l	UPort,a0
	jsr	WaitPort(a6)
	bra	MessageLoop

Exit:
	move.l	IntBase,a6
	move.l	WindowHD,a0
	jsr	CloseWindow(a6)		; Fenster schlie�en

WinError:
	move.l	ExecBase,a6
	move.l	IntBase,a1
	jsr	CloseLib(a6)		; Lib schlie�en

IntError:
	rts


MessageBranch:
	move.l	Message,a0		; Nachricht auswerten
	move.l	20(a0),Class

	move.l	Message,a1
	jsr	ReplyMsg(a6)		; und best�tigen

	cmp.l	#$200,Class
	beq	Exit

	bra	MessageLoop

* Datenbereich

Class:		dc.l	0
Message:	dc.l	0
UPort:		dc.l	0
RPort:		dc.l	0
IntBase:	dc.l	0
WindowHD:	dc.l	0

IntName:	dc.b	"intuition.library",0	
	even

WinName:	dc.b	"Das Close-Gadget beendet das Programm !",0
	even

WindowArgs:
	dc.w	150,50,340,90
	dc.b	1,3
	dc.l	$200,$1100F,0,0,WinName
ScreenHD
	dc.l	0,0
	dc.w	100,50,200,100,1


* IntuiText-Struktur


IText0:
	dc.b	1,3
	dc.b	0
	even
	dc.w	0,0
	dc.l	0
	dc.l	ITextData0
	dc.l	IText1

ITextData0:
	dc.b	"Tolles Bild oder? =>",0
	even

IText1:
	dc.b	2,3
	dc.b	0
	even
	dc.w	-1,-1
	dc.l	0
	dc.l	ITextData0
	dc.l	0

***************************************
* Border-Data

Border:
	dc.w	0,0
	dc.b	2,0
	dc.b	0,5
	dc.l	BorderData
	dc.l	Border0

BorderData:
	dc.w	0,0
	dc.w	270,0
	dc.w	270,30
	dc.w	0,30
	dc.w	0,0

Border0:
	dc.w	1,1
	dc.b	1,0,0,3
	dc.l	BorderData0,0

BorderData0:
	dc.w	270,0
	dc.w	270,30
	dc.w	2,30

***************************************
* Image-Data

Image:
	dc.w	0,0
	dc.w	32,20
	dc.w	2
	dc.l	ImageData
	dc.b	%11,0
	dc.l	0

	Section	"",Data_C

ImageData:

	dc.l	%11111111111111111111111111111111
	dc.l	%10000000000000000000000000000000
	dc.l	%10000000000000000000000000001000
	dc.l	%10000000000000000000000000001000
	dc.l	%10000000000000000000000000001000
	dc.l	%10000000000000000000000000001000
	dc.l	%10000000001111111111110000001000
	dc.l	%10000000001111111111100000001000
	dc.l	%10000000001111111111100000001000
	dc.l	%10000000001111111111100000001000
	dc.l	%10000000001111111111100000001000
	dc.l	%10000000001111111111100000001000
	dc.l	%10000000001000000000000000001000
	dc.l	%10000000000000000000000000001000
	dc.l	%10000000000000000000000000001000
	dc.l	%10000000000000000000000000001000
	dc.l	%10000000000000000000000000001000
	dc.l	%10001111111111111111111111111000
	dc.l	%10000000000000000000000000000000
	dc.l	%10000000000000000000000000000000

	dc.l	%00000000000000000000000000000000
	dc.l	%00000000000000000000000000000001
	dc.l	%00011111111111111111111111111001
	dc.l	%00010000000000000000000000000001
	dc.l	%00010000000000000000000000000001
	dc.l	%00010000000000000000000000000001
	dc.l	%00010000000000000000000000000001
	dc.l	%00010000000111111111110000000001
	dc.l	%00010000000111111111110000000001
	dc.l	%00010000000111111111110000000001
	dc.l	%00010000000111111111110000000001
	dc.l	%00010000000111111111110000000001
	dc.l	%00010000000111111111110000000001
	dc.l	%00010000001111111111110000000001
	dc.l	%00010000000000000000000000000001
	dc.l	%00010000000000000000000000000001
	dc.l	%00010000000000000000000000000001
	dc.l	%00010000000000000000000000000001
	dc.l	%00000000000000000000000000000001
	dc.l	%11111111111111111111111111111111

* Ende


*
* Kapitel 5
* Demonstrationsprogramm f�r Requester
*

ExecBase		=	4
OldOpenLib		=	-408
CloseLib		=	-414
ReplyMsg		=	-378
WaitPort		=	-384
GetMsg			=	-372
OpenWindow		=	-204
CloseWindow		=	-72
InitRequest		=	-138
Request			=	-240

Start:
	move.l	ExecBase,a6

	lea	IntName,a1
	jsr	OldOpenLib(a6)
	move.l	d0,IntBase
	beq	IntError

	move.l	IntBase,a6
	lea	WindowArgs,a0
	jsr	OpenWindow(a6)
	move.l	d0,WindowHD
	beq	WinError

	move.l	WindowHD,a0
	move.l	86(a0),UPort

	move.w	#100,rq_LeftEdge
	move.w	#040,rq_TopEdge

	move.w	#34*8,rq_Width
	move.w	#76,rq_Height

	move.l	#EndGadget,rq_ReqGadget
	move.l	#BitMap,rq_ImageBitMap
	move.w	#2,rq_Flags
	move.w	#2,rq_BackFill

	move.l	IntBase,a6
	lea	Requester,a0
	move.l	WindowHD,a1
	jsr	Request(a6)

MessageLoop:
	move.l	ExecBase,a6

	move.l	UPort,a0
	jsr	GetMsg(a6)
	move.l	d0,Message
	bne	MessageBranch

	move.l	UPort,a0
	jsr	WaitPort(a6)
	bra	MessageLoop

Exit:	move.l	IntBase,a6
	move.l	WindowHD,a0
	jsr	CloseWindow(a6)

WinError:
	move.l	ExecBase,a6
	move.l	IntBase,a1
	jsr	CloseLib(a6)
IntError:
	rts


MessageBranch:
	move.l	Message,a0
	move.l	20(a0),Class
	move.l	28(a0),a0
	move.w	38(a0),GadID

	cmp.l	#$800,Class
	bne	NoReq
	bset	#1,Flag

NoReq:	move.l	Message,a1
	jsr	ReplyMsg(a6)

	btst	#1,Flag
	bne	ReqMessage

	cmp.l	#$200,Class
	beq	Exit

	cmp.l	#$10000,Class
	beq	Exit

	bra	MessageLoop


ReqMessage:
	cmp.l	#$1000,Class
	bne	OhNo
	bclr	#1,Flag
OhNo:
	cmp.l	#$10000,Class
	bne	OhNo1
	bclr	#1,Flag
OhNo1:
	bra	MessageLoop

* Datenbereich

GadID:		dc.w	0
Class:		dc.l	0
Flag:		dc.w	$FFFF
IntBase:	dc.l	0
WindowHD:	dc.l	0
Message:	dc.l	0
UPort:		dc.l	0

IntName:	dc.b	"intuition.library",0
	even
WinName:	dc.b	"Window",0
	even

WindowArgs:
	dc.w	0,0,640,256
	dc.b	1,3
	dc.l	$11A00,$0100F,0,0,WinName,0,0
	dc.w	100,50,200,100,1

*************************

Requester:	dc.l	0
rq_LeftEdge:	dc.w	0
rq_TopEdge:	dc.w	0
rq_Width:	dc.w	0
rq_Height:	dc.w	0
		dc.w	0
		dc.w	0
rq_ReqGadget:	dc.l	0
		dc.l	0
		dc.l	0
rq_Flags:	dc.w	0
rq_BackFill:	dc.b	0
		even
		dc.l	0
		dcb.b	32,0
rq_ImageBitMap:	dc.l	0
rq_RWindow:	dc.l	0
		dcb.b	36,0
		dcb.b	50,0

*************************

EndGadget:
	dc.l	OKGadget
	dc.w	189
	dc.w	25
	dc.w	75
	dc.w	18
	dc.w	0
	dc.w	$05
	dc.w	1
	dc.l	0
	dc.l	0
	dc.l	0
	dc.l	0
	dc.l	0
	dc.w	1
	dc.l	0

OKGadget:
	dc.l	FirstGadget
	dc.w	189
	dc.w	44
	dc.w	75
	dc.w	18
	dc.w	0
	dc.w	$05
	dc.w	1
	dc.l	0
	dc.l	0
	dc.l	0
	dc.l	0
	dc.l	0
	dc.w	2
	dc.l	0

FirstGadget:
	dc.l	SecondGadget
	dc.w	12
	dc.w	32
	dc.w	15
	dc.w	8
	dc.w	0
	dc.w	$102
	dc.w	1
	dc.l	0
	dc.l	0
	dc.l	0
	dc.l	0
	dc.l	0
	dc.w	3
	dc.l	0

SecondGadget:
	dc.l	0
	dc.w	12
	dc.w	46
	dc.w	15
	dc.w	8
	dc.w	0
	dc.w	$102
	dc.w	1
	dc.l	0
	dc.l	0
	dc.l	0
	dc.l	0
	dc.l	0
	dc.w	3
	dc.l	0


************************

BitMap:
    	dc.w	34		; Bytes pro Reihe
	dc.w	76    		; Reihen
	dc.b	0		; Flags
	dc.b	2 		; Teife
	dc.w	0 	   	; 
	dc.l	BitMapData	; 
	dc.l	BitMapData+34*76; 

	Section	"",Data_C

BitMapData:
	incbin	PrgDisk:Kapitel_5/Requester.bm
	
* Ende





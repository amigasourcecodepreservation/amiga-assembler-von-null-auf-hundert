
* Programm 7.2: Demonstration SetAPen, WritePixel, Move, Draw

ExecBase	=	4
OpenLib	=	-552
CloseLib	=	-414
GetRGB4	=	-582
LoadRGB4	=	-192
OpenWindow	=	-204
WaitPort	=	-384
GetMsg	=	-372
ReplyMsg	=	-378
SetAPen	=	-342
WritePixel	=	-324
Move	=	-240
Draw	=	-246
CloseWindow	=	-72

	move.l	4,a6	; Intuition-Lib �ffnen
	lea	intname,a1
	clr.l	d0
	jsr	OpenLib(a6)
	move.l	d0,intbase

	lea	gfxname,a1	; Graphics-Lib �ffnen
	clr.l	d0
	jsr	OpenLib(a6)
	move.l	d0,gfxbase

	move.l	intbase,a6

	lea	nwindow,a0	; Window �ffnen
	jsr	OpenWindow(a6)
	move.l	d0,window

	move.l	d0,a0	; Window nach a0
	move.l	$2e(a0),a0	; Zeiger auf Screen
	lea	$2c(a0),a0	; Jetzt zum ViewPort
	move.l	a0,a5	; ViewPort merken
	move.l	4(a0),a4	; Zeiger auf ColorMap merken

	move.l	gfxbase,a6	; Benutze Graphics-Lib

	moveq	#0,d4	; Z�hler f�r Farben
	lea	oldcol,a3	; Zeiger auf Feld f�r alte Farben
main2:	move.l	a4,a0	; ColorMap nach a0
	move.l	d4,d0	; Farbnummer
	jsr	GetRGB4(a6)
	move.w	d4,d1
	asl.w	#1,d1
	move.w	d0,0(a3,d1.w)	; Farbe in Farbfeld eintragen
	addq	#1,d4
	cmp.b	#4,d4
	blt	main2

	move.l	a5,a0	; ViewPort-Zeiger nach a1
	lea	colors,a1	; Zeiger auf Farbfeld nach a1
	moveq	#4,d0	; Setze 4 Farben
	jsr	LoadRGB4(a6)	; LoadRGB4 aufrufen

	move.l	window,a0	; Window-Zeiger nach a0
	move.l	$32(a0),a4	; Rastport-Zeiger holen
	move.l	$56(a0),a3	; Replyport-Zeiger holen

main1:	move.l	ExecBase,a6

	move.l	a3,a0	; Window-Replyport
	jsr	WaitPort(a6)	; Warte auf Nachricht

	move.l	a3,a0	; Hole Message ab
	jsr	GetMsg(a6)
	move.l	d0,a1	; Message-Zeiger nach a1
	move.l	$14(a1),d4	; Class nach d4
	move.w	$18(a1),d5	; Code nach d5
	move.w	$20(a1),d6	; Mouse-X nach d6
	move.w	$22(a1),d7	; Mouse-Y nach d7

	jsr	ReplyMsg(a6)	; Message quittieren

	cmp.l	#$8,d4	; War es MOUSEBUTTON?
	beq	mbutton	; Wenn ja
	cmp.l	#$10,d4	; War es MOUSEMOVE?
	beq	mmove	; Wenn ja
	cmp.l	#$200,d4	; War es CLOSEWINDOW?
	beq	finish	; Wenn ja
	bra	main1	; Ansonsten zur Hauptschleife

mbutton:	cmp.l	#$68,d5	; Linke Taste gedr�ckt?
	beq	mb1	; Wenn ja
	cmp.l	#$e8,d5	; Linke Taste losgelassen?
	beq	mb2	; Wenn ja
	cmp.l	#$69,d5	; Rechte Taste gedr�ckt?
	bne	mb3	; Wenn nein

	add.b	#1,drawcol	; N�chste Farbe anw�hlen
	cmp.b	#3,drawcol	; H�chste Farbnummer �berschritten?
	ble	mb3	; Wenn nein
	move.b	#0,drawcol	; Zur Farbe 0 zur�ck
	bra	mb3

mb1:	move.b	#1,drawon	; Zeichnen aktivieren

	move.l	gfxbase,a6
	move.l	a4,a1	; Rastport nach a1
	move.b	drawcol,d0	; Farbe nach d0
	jsr	SetAPen(a6)	; Farbe setzen

	move.l	a4,a1	; Rastport
	move.w	d6,d0	; Maus-X-Position
	move.w	d7,d1	; Maus-Y-Position
	jsr	WritePixel(a6); Punkt zeichnen

	move.l	a4,a1	; Rastport
	move.w	d6,d0	; X-Pos
	move.w	d7,d1	; Y-Pos
	jsr	Move(a6)	; Zeichenstift repositionieren
	bra	main1

mb2:	clr.b	drawon	; Zeichnen deaktivieren

mb3:	bra	main1	; Zur Hauptschleife

mmove:	tst.b	drawon	; Zeichnen aktiviert?
	beq	mm1	; Wenn nein

	move.l	gfxbase,a6
	move.l	a4,a1	; Rastport nach a1
	move.b	drawcol,d0	; Farbe nach d0
	jsr	SetAPen(a6)	; Farbe setzen

	move.l	a4,a1	; Rastport
	move.w	d6,d0	; Maus-X-Position
	move.w	d7,d1	; Maus-Y-Position
	jsr	Draw(a6)	; Linie zum neuen Punkt ziehen

mm1:	bra	main1	; Zur Hauptschleife

finish:	move.l	gfxbase,a6

	move.l	a5,a0	; ViewPort-Zeiger nach a1
	lea	oldcol,a1	; Zeiger auf alte Farben
	moveq	#4,d0	; Setze 4 Farben
	jsr	LoadRGB4(a6)	; LoadRGB4 aufrufen

	move.l	intbase,a6

	move.l	window,a0	; Window schlie�en
	jsr	CloseWindow(a6)

	move.l	ExecBase,a6

	move.l	gfxbase,a1	; Libraries schlie�en
	jsr	CloseLib(a6)
	move.l	intbase,a1
	jsr	CloseLib(a6)

	rts		; Tsch��!

intname:	dc.b	"intuition.library",0
	even
gfxname:	dc.b	"graphics.library",0
	even
intbase:	dc.l	0
gfxbase:	dc.l	0

drawon:	dc.b	0	; Flag f�r Zeichnen-Aktivierung
drawcol:	dc.b	1	; Gew�hlte Zeichen-Farbe

nwindow:	dc.w	0,0,640,256
	dc.b	0,1
	dc.l	$218	; IDCMP: CLOSEWINDOW, MOUSEBUTTONS,
			; MOUSEMOVE
	dc.l	$1120f	; Flags: REPORTMOUSE, RMBTRAP,
			; ACTIVATE, alle System-Gadgets
	dc.l	0,0
	dc.l	wtitle
	dc.l	0
	dc.l	0
	dc.w	200,100,640,200
	dc.w	1
wtitle:	dc.b	"<- Close-Gadget klicken zum Beenden",0
	even

window:	dc.l	0

colors:	dc.w	$000,$fff,$f05,$4f4
oldcol:	ds.w	4


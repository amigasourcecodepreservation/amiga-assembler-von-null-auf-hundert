
* Programm 7.7: Benutzung der Area-Befehle

ExecBase	=	4
OpenLib	=	-552
CloseLib	=	-414
OpenScreen	=	-198
OpenWindow	=	-204
AllocRaster	=	-492
FreeRaster	=	-498
InitTmpRas	=	-468
InitArea	=	-282
SetAPen	=	-342
AreaMove	=	-252
AreaDraw	=	-258
AreaEllipse	=	-186
AreaEnd	=	-264
WaitPort	=	-384
GetMsg	=	-372
ReplyMsg	=	-378
CloseWindow	=	-72
CloseScreen	=	-66

	move.l	4,a6

	lea	intname,a1	: Intuition-Lib �ffnen
	clr.l	d0
	jsr	OpenLib(a6)
	move.l	d0,intbase

	lea	gfxname,a1	; Graphics-Lib �ffnen
	clr.l	d0
	jsr	OpenLib(a6)
	move.l	d0,gfxbase

	move.l	intbase,a6

	lea	nscreen,a0	; Screen �ffnen
	jsr	OpenScreen(a6)
	move.l	d0,wscreen

	lea	nwindow,a0	; Window �ffnen
	jsr	OpenWindow(a6)
	move.l	d0,window

	move.l	d0,a0	; Rastport- und Userport-Zeiger
	move.l	$32(a0),a4	; sichern
	move.l	$56(a0),a5

	move.l	gfxbase,a6

	move.l	#320,d0	; Speicher f�r TmpRas holen
	move.l	#256,d1 
	jsr	AllocRaster(a6)
	tst.l	d0
	beq	ende
	move.l	d0,rasmem

	lea	tmpras,a0	; Tempor�ren Rastport einrichten
	move.l	rasmem,a1
	move.l	#10240,d0
	jsr	InitTmpRas(a6)
	move.l	d0,12(a4)	

* AreaInfo-Struktur einrichten

	lea	areainfo,a0	; Zeiger auf Speicher f�r Struktur
	lea	vectable,a1	; Speicher f�r Punkt-Koordinaten
	move.l	#100,d0	; Maximal 100 Punkte
	jsr	InitArea(a6)	; Struktur einrichten

	lea	areainfo,a0	; AreaInfo-Zeiger in
	move.l	a0,16(a4)	; Rastport eintragen

	move.l	a4,a1	; Farbe 1 einstellen
	move.l	#1,d0
	jsr	SetAPen(a6)

* Achteck zeichnen

	move.l	a4,a1	; Rastport nach a1
	move.l	#145,d0	; X-Koordinate 145
	move.l	#175,d1	; Y-Koordinate 175
	jsr	AreaMove(a6)	; Area-Move

	move.l	a4,a1	; Area-Draw nach 175/175
 	move.l	#175,d0
	move.l	#175,d1
	jsr	AreaDraw(a6)

	move.l	a4,a1	; Area-Draw nach 196/196
	move.l	#196,d0
	move.l	#196,d1
	jsr	AreaDraw(a6)

	move.l	a4,a1	; usw.
	move.l	#196,d0
	move.l	#226,d1
	jsr	AreaDraw(a6)

	move.l	a4,a1
	move.l	#175,d0
	move.l	#247,d1
	jsr	AreaDraw(a6)

	move.l	a4,a1
	move.l	#145,d0
	move.l	#247,d1
	jsr	AreaDraw(a6)

	move.l	a4,a1
	move.l	#124,d0
	move.l	#226,d1
	jsr	AreaDraw(a6)

	move.l	a4,a1
	move.l	#124,d0
	move.l	#196,d1
	jsr	AreaDraw(a6)

	move.l	a4,a1
	move.l	#145,d0
	move.l	#175,d1
	jsr	AreaDraw(a6)

* Ellipse zeichnen

	move.l	a4,a1	; Rastport
	move.l	#150,d0	; X-Koordinate Mittelpunkt
	move.l	#70,d1	; Y-Koordinate Mittelpunkt
	move.l	#100,d2	; X-Radius
	move.l	#30,d3	; Y-Radius
	jsr	AreaEllipse(a6)	; Zeichnen

	move.l	a4,a1	; Rastport
	jsr	AreaEnd(a6)	; Alle Objekte zeichnen

	move.l	ExecBase,a6

	move.l	a5,a0	; Auf CLOSEWINDOW warten
	jsr	WaitPort(a6)
	move.l	a5,a0
	jsr	GetMsg(a6)
	move.l	d0,a1
	jsr	ReplyMsg(a6)

	move.l	gfxbase,a6

	clr.l	12(a4)	; TmpRas-Eintrag l�schen

	move.l	rasmem,a0	; Rasterspeicher freigeben
	move.l	#320,d0
	move.l	#256,d1
	jsr	FreeRaster(a6)

	move.l	intbase,a6

	move.l	window,a0	; Window und Screen schlie�en
	jsr	CloseWindow(a6)
	move.l	wscreen,a0
	jsr	CloseScreen(a6)

ende:	move.l	ExecBase,a6	; Libs schlie�en und Ende

	move.l	gfxbase,a1
	jsr	CloseLib(a6)
	move.l	intbase,a1
	jsr	CloseLib(a6)

	rts

intname:	dc.b	"intuition.library",0
	even
gfxname:	dc.b	"graphics.library",0
	even
intbase:	dc.l	0
gfxbase:	dc.l	0

nscreen:	dc.w	0,0,320,256
	dc.w	2
	dc.b	0,1
	dc.w	0,15
	dc.l	0,0,0,0

nwindow:	dc.w	0,0,320,256
	dc.b	0,1
	dc.l	$200
	dc.l	$100f
	dc.l	0,0
	dc.l	wtitle
wscreen:	dc.l	0,0
	dc.w	200,100,640,200
	dc.w	15
wtitle:	dc.b	"<- Klick zum Beenden",0
	even

window:	dc.l	0

areainfo:	ds.b	24	; Speicher f�r AreaInfo-Struktur
vectable:	ds.b	500	; 500 Bytes f�r Koordinaten

rasmem:	dc.l	0	; F�r Rasterspeicher-Startadresse
tmpras:	ds.b	8	; Speicher f�r TmpRas-Struktur



* Programm 7.10: Demonstration ClipBlit und BltBitMap

ExecBase	=	4
OpenLib	=	-552
CloseLib	=	-414
OpenWindow	=	-204
WaitPort	=	-384
GetMsg	=	-372
ReplyMsg	=	-378
CloseWindow	=	-72
SetAPen	=	-342
DrawEllipse	=	-180
Flood	=	-330
ClipBlit	=	-552
BltBitMap	=	-30
Delay	=	-198

	move.l	ExecBase,a6	; Libs �ffnen
	lea	intname,a1
	clr.l	d0
	jsr	OpenLib(a6)
	move.l	d0,intbase
	lea	gfxname,a1
	clr.l	d0
	jsr	OpenLib(a6)
	move.l	d0,gfxbase
	lea	dosname,a1
	clr.l	d0
	jsr	OpenLib(a6)
	move.l	d0,dosbase

	move.l	intbase,a6

	lea	nwindow1,a0	; Window 1 �ffnen
	jsr	OpenWindow(a6)
	move.l	d0,a4

	lea	nwindow2,a0	; Window 2 �ffnen
	jsr	OpenWindow(a6)
	move.l	d0,a5

	move.l	gfxbase,a6

	move.l	$32(a4),a3	; Rastport von Window 1 nach a3

	move.l	a3,a1	; Zeichenfarbe eins setzen
	moveq	#1,d0
	jsr	SetAPen(a6)

	move.l	a3,a1	; Erste Ellipse zeichnen
	moveq	#95,d0
	moveq	#42,d1
	moveq	#50,d2
	moveq	#25,d3
	jsr	DrawEllipse(a6)

	move.l	a3,a1	; Zweite Ellipse zeichnen
	move.l	#175,d0
	moveq	#42,d1
	moveq	#50,d2
	moveq	#25,d3
	jsr	DrawEllipse(a6)

	move.b	#1,27(a3)	; F�llrandfarbe: 1

	move.l	a3,a1	; Linke Ellipse in Farbe 1 ausf�llen
	moveq	#95,d0
	moveq	#42,d1
	moveq	#0,d2
	jsr	Flood(a6)

	move.l	a3,a1	; Farbe: 2
	moveq	#2,d0
	jsr	SetAPen(a6)

	move.l	a3,a1	; Rechte Ellipse ausf�llen
	move.l	#175,d0
	moveq	#42,d1
	moveq	#0,d2
	jsr	Flood(a6)

	move.l	a3,a1	; Farbe: 3
	moveq	#3,d0
	jsr	SetAPen(a6)

	move.l	a3,a1	; Schnittfl�che der Ellipsen ausf�llen
	move.l	#140,d0
	moveq	#42,d1
	moveq	#0,d2
	jsr	Flood(a6)

	bsr	verz	; Verz�gerung per Delay

* 1:1-Kopie des Windows 1 ins Window 2

	move.l	a3,a0	; Quell-Rastport
	moveq	#0,d0	; Quell-Startkoordinaten
	moveq	#0,d1
	move.l	$32(a5),a1	; Ziel-Rastport
	moveq	#0,d2	; Ziel-Koordinaten
	moveq	#0,d3
	move.l	#300,d4	; Breite und H�he
	move.l	#100,d5	; der Grafik
	move.b	#192,d6	; Minterm-Modus = 1:1-Kopie
	jsr	ClipBlit(a6)	

	bsr	verz

* Invertierte Kopie von Window 2 in Window 1

	move.l	$32(a5),a0	; Quell-Rastport
	moveq	#0,d0	; Quell-Koordinaten
	moveq	#0,d1
	move.l	a3,a1	; Ziel-Rastport
	moveq	#0,d2	; Ziel-Koordinaten
	moveq	#0,d3
	move.l	#300,d4	; Breite und H�he
	move.l	#100,d5
	move.b	#48,d6	; Minterm-Modus = Invertiere Kopie
	jsr	ClipBlit(a6)	

	bsr	verz

* OR-Verkn�pfung der ersten Planes der beiden Windows

	move.l	$32(a4),a0	; Quelle = Window 1
	move.l	4(a0),a0	; Vom Rastport zur Bitmap
	moveq	#5,d0	; Start-X = 5
	moveq	#15,d1	; Start-Y = 15
	move.l	$32(a5),a1	; Ziel = Window 2
	move.l	4(a1),a1	; Zur Bitmap
	move.l	#315,d2	; Ziel-Koordinaten
	move.l	#120,d3
	move.l	#290,d4	; Breite
	move.l	#70,d5	; H�he
	move.b	#224,d6	; Minterm-Modus: OR-Verkn�pfung
	move.b	#1,d7	; Nur Plane 1 betroffen
	lea	buff,a2	; Zwischenspeicher
	jsr	BltBitMap(a6)	

	bsr	verz

* EOR-Verkn�pfung

	move.l	a3,a0	; Quelle = Window 1
	moveq	#0,d0	; Koordinaten
	moveq	#0,d1
	move.l	$32(a5),a1	; Ziel = Window 2
	moveq	#0,d2	; Koordinaten
	moveq	#0,d3
	move.l	#300,d4	; Breite
	move.l	#100,d5	; H�he
	move.b	#96,d6	; Minterm-Modus: EOR-Verkn�pfung
	jsr	ClipBlit(a6)	

	move.l	ExecBase,a6

	move.l	$56(a4),a0	; Auf CLOSEWINDOW im Window 1 warten
	jsr	WaitPort(a6)
	move.l	$56(a4),a0
	jsr	GetMsg(a6)
	move.l	d0,a1
	jsr	ReplyMsg(a6)

	move.l	intbase,a6

	move.l	a4,a0	; Windows schlie�en
	jsr	CloseWindow(a6)

	move.l	a5,a0
	jsr	CloseWindow(a6)

	move.l	ExecBase,a6

	move.l	gfxbase,a1	; Libs schlie�en und Ende
	jsr	CloseLib(a6)
	move.l	intbase,a1
	jsr	CloseLib(a6)
	move.l	dosbase,a1
	jsr	CloseLib(a6)

	rts

verz:	move.l	dosbase,a6	; Warte 100 50stel Sekunden
	moveq	#100,d1	; (also zwei Sekunden)
	jsr	Delay(a6)
	move.l	gfxbase,a6
	rts

intname:	dc.b	"intuition.library",0
	even
gfxname:	dc.b	"graphics.library",0
	even
dosname:	dc.b	"dos.library",0
	even
intbase:	dc.l	0
gfxbase:	dc.l	0
dosbase:	dc.l	0

nwindow1:	dc.w	0,0,300,100
	dc.b	0,1
	dc.l	$200
	dc.l	$40f
	dc.l	0,0
	dc.l	wtitle1
	dc.l	0,0
	dc.w	300,100,300,100
	dc.w	1
wtitle1:	dc.b	"<- Hier klicken zum Beenden",0
	even

nwindow2:	dc.w	310,105,300,100
	dc.b	0,1
	dc.l	$200
	dc.l	$40f
	dc.l	0,0
	dc.l	wtitle2
	dc.l	0,0
	dc.w	300,100,300,100
	dc.w	1
wtitle2:	dc.b	"Demo ClipBlit und BltBitMap",0
	even

buff:	ds.b	80


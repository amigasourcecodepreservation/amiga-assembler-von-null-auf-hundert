*
* Kapitel 10
* Demonstrationsprogramm f�r das Console-Device
*

ExecBase	=	4
CreatePort	=	-30
DeletePort	=	-36
CreateIOReq	=	-42
DeleteIOReq	=	-48
OpenLib		=	-552
CloseLib	=	-414
OpenWindow	=	-204
CloseWindow	=	-72
OpenDev		=	-444
CloseDev	=	-450
SendIO		=	-462
DoIO		=	-456

Start:	move.l	ExecBase,a6
	lea	IOName,a1
	moveq	#0,d0
	jsr	OpenLib(a6)
	move.l	d0,IOBase

	lea	IntName,a1
	moveq	#0,d0
	jsr	OpenLib(a6)
	move.l	d0,IntBase

	move.l	IntBase,a6
	lea	WindowArgs,a0
	jsr	OpenWindow(a6)
	move.l	d0,WindowHD

	move.l	IOBase,a6
	moveq	#0,d0
	sub.l	a0,a0
	jsr	CreatePort(a6)	; Reply-Port f�r Read erstellen

	move.l	d0,a0
	move.l	#48,d0
	jsr	CreateIOReq(a6)	; IOReq f�r Read ertellen
	move.l	d0,IORReq

	move.l	IOBase,a6
	moveq	#0,d0
	sub.l	a0,a0
	jsr	CreatePort(a6)	; Reply-Port f�r Write erstellen

	move.l	d0,a0
	move.l	#48,d0
	jsr	CreateIOReq(a6)	; IOReq f�r Write erstellen
	move.l	d0,IOWReq

	move.l	ExecBase,a6
	move.l	IORReq,a1
	move.l	WindowHD,40(a1)
	moveq	#0,d0
	moveq	#0,d1
	lea	ConName,a0
	jsr	OpenDev(a6)	; Device �ffnen

	move.l	IORReq,a0	; 
	move.l	IOWReq,a1
	move.l	20(a0),20(a1)	; Device und
	move.l	24(a0),24(a1)	; Unit �bertragen


Loop:	lea	Text,a0
	bsr	WriteStr

	bsr	ReadStr
	cmp.l	#"exit",Puffer
	bne	Loop
	

	move.l	ExecBase,a6
	move.l	IORReq,a1
	jsr	CloseDev(a6)	; Device schlie�en

	move.l	IOBase,a6
	move.l	IOWReq,a0
	move.l	14(a0),a0
	jsr	DeletePort(a6)	; Write Reply-Port l�schen

	move.l	IOWReq,a0
	jsr	DeleteIOReq(a6)	; Write IOReq l�schen

	move.l	IORReq,a0
	move.l	14(a0),a0
	jsr	DeletePort(a6)	; Read Reply-Port l�schen

	move.l	IORReq,a0
	jsr	DeleteIOReq(a6)	; Read IOReq l�schen

	move.l	IntBase,a6
	move.l	WindowHD,a0
	jsr	CloseWindow(a6)

	move.l	ExecBase,a6
	move.l	IntBase,a1
	jsr	CloseLib(a6)

	move.l	ExecBase,a6
	move.l	IOBase,a1
	jsr	CloseLib(a6)
	rts

* Sub`s

ReadChar:
	move.l	a0,-(a7)
	move.l	ExecBase,a6
	move.l	IORReq,a1
	move.w	#2,28(a1)
	move.l	#RCPuffer,40(a1)
	move.l	#1,36(a1)
	jsr	DoIO(a6)
	moveq	#0,d0
	move.b	RCPuffer,d0
	move.l	(a7)+,a0
	rts

RCPuffer:	dc.w	0

ReadStr:lea	Puffer,a0
RSLoop:	bsr	ReadChar
	move.b	d0,(a0)
	moveq	#1,d0
	bsr	Write
	cmp.b	#13,(a0)+
	bne	RSLoop
	move.b	#0,(a0)+
	move.l	d1,d0
	rts

WriteStr:
	move.l	a0,a1
	moveq	#0,d0
	bra	WSEin
WSLoop:	add.l	#1,d0
WSEin:	tst.b	(a1)+
	bne	WSLoop

Write:	move.l	a0,-(a7)
	move.l	ExecBase,a6
	move.l	IOWReq,a1
	move.w	#3,28(a1)
	move.l	a0,40(a1)
	move.l	d0,36(a1)
	jsr	DoIO(a6)
	move.l	(a7)+,a0
	rts
	

* Datenbereich

IntName:	dc.b	"intuition.library",0
	even
IOName:		dc.b	"io.library",0
	even
ConName:	dc.b	"console.device",0
	even
IntBase:	dc.l	0
InpUnit:	dc.l	0
IOBase:		dc.l	0
IORReq:		dc.l	0
IOWReq:		dc.l	0
WindowHD:	dc.l	0

Text:		dc.b	10,"Enter exit!",10,"> ",0
	even
Puffer:		ds.b	60	

WindowArgs:
	dc.w	90,10,460,200
	dc.b	1,3
	dc.l	$200,$1100F,0,0,0
ScreenHD
	dc.l	0,0
	dc.w	100,50,200,100,1

* Ende


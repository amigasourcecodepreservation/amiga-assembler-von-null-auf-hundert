*
*	Kapitel 10
*	Quelltext der "conio.library"
*

ExecBase	=	4
OpenLib 	=	-552
CloseLib	=	-414
Output		=	-60
Write		=	-48
Remove		=	-252
FreeMem		=	-210
AllocMem	=	-198
AddPort		=	-354
RemPort		=	-360
AllocSignal	=	-330
FreeSignal	=	-336
FindPort	=	-390
OpenDevice	=	-444
CloseDevice	=	-450
CreatePort	=	-30
DeletePort	=	-36
CreateIOReq	=	-42
DeleteIOReq	=	-48
GetMsg		=	-372
ReplyMsg	=	-378
WaitPort	=	-384
OpenWindow	=	-204
CloseWindow	=	-72
OpenDev		=	-444
CloseDev	=	-450
SendIO		=	-462
DoIO		=	-456

Start:	move.l	ExecBase,a6	; DosLibrary �ffnen
	lea	DosName,a1
	moveq 	#0,d0
	jsr	OpenLib(a6)
	move.l	d0,a6
	beq	DosError

	jsr	Output(a6)	; Ausgabekanal ermitteln
	move.l	d0,d1

	move.l	#TextA,d2
	move.l	#TextE-TextA,d3
	jsr	Write(a6)
 
	move.l	a6,a1
	move.l	ExecBase,a6
	jsr	CloseLib(a6)

DosError:
	moveq  #0,d0
	rts

TextA:	dc.b	10,"�� ConIO-Library Version 0.1 (Ron) ��",10,10
TextE:
	even

LibResident:
	dc.w	$4AFC		; Resident-Struktur
	dc.l	LibResident
	dc.l	EndResident
	dc.b	%10000000
	dc.b	1
	dc.b	9
	dc.b	0
	dc.l	LibName
	dc.l	LibIDString
	dc.l	LibInitData

LibName:	dc.b	"conio.library",0
	even
LibIDString:	dc.b	"ConIO-Library v0.1",0
	even

LibInitData:
	dc.l	46	; LibSize (34) + SegList (4) + IOBase (4) + IntBase (4) = 46
	dc.l	FuncTab
	dc.l	DataTab
	dc.l	LibInitRout

FuncTab:
	dc.l	Open		;-6
	dc.l	Close		;-12 Standardfunktionen
	dc.l	Expunge		;-18
	dc.l	ExtFunc		;-24

	dc.l	InstallInp	;-30
	dc.l	RemoveInp	;-36

	dc.l	ReadChar	;-42
	dc.l	WriteChar	;-48

	dc.l	ReadCrL		;-54
	dc.l	ReadStr		;-60

	dc.l	WriteLeL	;-66
	dc.l	WriteStr	;-72
	dc.l	WriteXY		;-78
	dc.l	WriteDec	;-84
	dc.l	WriteHex	;-90
	
	dc.l	GotoXY		;-96
	dc.l	ScrollUp	;-102
	dc.l	ScrollDown	;-108

	dc.l	DelLine		;-114
	dc.l	InsLine		;-120

	dc.l	DelEOD		;-126
	dc.l	DelEOL		;-132

	dc.l	CursorOn	;-138
	dc.l	CursorOff	;-144

	dc.l	-1

DataTab:
	dc.b	%11100000,0	; Tabelle f�r InitStruct
	dc.w	8
	dc.b	9,0

	dc.b	%11000000,0
	dc.w	10
	dc.l	LibName

	dc.b	%11100000,0
	dc.w	14
	dc.b	6,0

	dc.b	%11000000,0
	dc.w	20
	dc.w	3,4

	dc.l	0

DosName:	dc.b "dos.library",0
	even
IOName:		dc.b "io.library",0
	even
IntName:	dc.b "intuition.library",0
	even

; Initialisierungsroutine

LibInitRout:
	movem.l	a5/a6,-(a7)

	move.l	d0,a5		; Library-Basis nach a5
	move.l	a0,34(a5)	; Segment-Liste eintragen

	move.l	ExecBase,a6
	moveq	#0,d0
	lea	IOName(pc),a1
	jsr	OpenLib(a6)
	move.l	d0,38(a5)
	beq	LibInitEnd

	moveq	#0,d0
	lea	IntName(pc),a1
	jsr	OpenLib(a6)
	move.l	d0,42(a5)
	beq	LibInitEnd

	move.l	a5,d0
LibInitEnd:
	movem.l	(a7)+,a5/a6
	rts

; Open-Routine

Open:	
	bclr	#3,14(a6)	; ExpungBit = 0
	addq.w	#1,32(a6)	; OpenCnt ++
	move.l	a6,d0
	rts

; Close-Routine

Close:	moveq	#0,d0

	subq.w	#1,32(a6)	; OpenCnt --
	bne	CloseEnd

*	btst	#3,14(a6)	; ExpungBit = 1 ?
*	beq	CloseEnd

	bsr	Expunge		; entfernen

CloseEnd:
	rts

; Expunge-Routine

Expunge:
	movem.l	d1-d6/a0-a6,-(a7)

	tst.w	32(a6)		; OpenCnt = 0 ?
	beq	ExpungeBranch

	moveq	#0,d0		; Nein:
	bset	#3,14(a6)	
	bra	ExpungeEnd

ExpungeBranch:
	move.l	a6,a4		; Ja:
	move.l	ExecBase,a6
	move.l	38(a4),a1
	jsr	CloseLib(a6)

	move.l	42(a4),a1
	jsr	CloseLib(a6)

	move.l	a4,a1		; Lib aus Liste nehmen
	jsr	Remove(a6)	; Remove

	move.l	34(a4),d2	; SegmentList retten

	moveq	#0,d0		; Library-Speicher freigeben
	move.w	16(a4),d0
	move.l	a4,a1
	sub.l	d0,a1
	add.w	18(a4),d0
	jsr	FreeMem(a6)	; FreeMem

	move.l	d2,d0		; SegmentList zur�ckgeben

ExpungeEnd:
	movem.l	(a7)+,d1-d6/a0-a6
	rts

; ExtFunc-Routine

ExtFunc:
	moveq	#0,d0		; R�ckgabewert = Null
	rts

;
; ConIO-Unit
;
; 000 WindowPtr
; 004 ReadIOReq
; 008 WriteIOReq
; 012 TextPuffer
;
;
;

ConName:	dc.b	"console.device",0
	even


*
* InstallInp
*
* a0 > WindowPtr (Zeiger auf Fenster)
* 
*
*

InstallInp:
	movem.l	d1-d7/a0-a6,-(a7)

	move.l	a6,a5
	move.l	a0,d7

	move.l	ExecBase,a6
	move.l	#12,d0
	move.l	#0,d1
	jsr	AllocMem(a6)
	move.l	d0,a4
	beq	MemError

	move.l	42(a5),a6
	move.l	d7,a0
	jsr	OpenWindow(a6)
	move.l	d0,(a4)
	beq	WinError

	move.l	38(a5),a6
	moveq	#0,d0
	sub.l	a0,a0
	jsr	CreatePort(a6)	; Reply-Port f�r Read erstellen
	move.l	d0,a0
	beq	RPError

	move.l	#48,d0
	jsr	CreateIOReq(a6)	; IOReq f�r Read ertellen
	move.l	d0,4(a4)
	beq	RRError

	moveq	#0,d0
	sub.l	a0,a0
	jsr	CreatePort(a6)	; Reply-Port f�r Write erstellen
	move.l	d0,a0
	beq	WPError

	move.l	#48,d0
	jsr	CreateIOReq(a6)	; IOReq f�r Write erstellen
	move.l	d0,8(a4)
	beq	WRError

	move.l	ExecBase,a6
	move.l	4(a4),a1
	move.l	(a4),40(a1)
	moveq	#0,d0
	moveq	#0,d1
	lea	ConName,a0
	jsr	OpenDev(a6)	; Device �ffnen
	tst.l	d0
	bne	DevError

	move.l	4(a4),a0	; 
	move.l	8(a4),a1	; 
	move.l	20(a0),20(a1)	; Device und
	move.l	24(a0),24(a1)	; Unit �bertragen

	move.l	a4,d0
IIEnd:	movem.l	(a7)+,d1-d7/a0-a6
	rts

IIEndErr:
	movem.l	(a7)+,d1-d7/a0-a6
	moveq	#0,d0
	rts

*
* RemoveInp
*
* a0 < Zeiger auf ConIO-Unit
*
*

RemoveInp:
	movem.l	d1-d7/a0-a6,-(a7)
	move.l	a0,a4
	move.l	a6,a5

	move.l	ExecBase,a6
	move.l	4(a4),a1
	jsr	CloseDev(a6)	; Device schlie�en

DevError:
	move.l	38(a5),a6
	move.l	8(a4),a0
	move.l	14(a0),d7
	jsr	DeleteIOReq(a6)	; Write IOReq l�schen
	move.l	d7,a0

WRError:
	jsr	DeletePort(a6)	; Write Reply-Port l�schen

WPError:
	move.l	4(a4),a0
	move.l	14(a0),d7
	jsr	DeleteIOReq(a6)	; Read IOReq l�schen
	move.l	d7,a0

RRError:
	jsr	DeletePort(a6)	; Read Reply-Port l�schen

RPError:
	move.l	42(a5),a6
	move.l	(a4),a0
	jsr	CloseWindow(a6)

WinError:
	move.l	ExecBase,a6
	move.l	#12,d0
	move.l	a4,a1
	jsr	FreeMem(a6)

MemError:
	movem.l	(a7)+,d1-d7/a0-a6
	moveq	#0,d0
	rts

*
* ReadChar
*
* d0 > gelesenes Zeichen
*

ReadChar:
	movem.l	a0-a1/a4/a6,-(a7)
	move.l	ExecBase,a6
	move.l	4(a5),a1
	move.w	#2,28(a1)
	lea	Puffer,a4
	move.l	a4,40(a1)
	move.l	#1,36(a1)
	jsr	DoIO(a6)

	moveq	#0,d0
	move.b	(a4),d0
	movem.l	(a7)+,a0-a1/a4/a6
	rts

*
* WriteChar
*
* d0 < Byte-Wert, der ausgegeben werden soll
*

WriteChar
	movem.l	d0/a0-a1/a4/a6,-(a7)
	lea	Puffer,a4
	move.b	d0,(a4)
	move.l	8(a5),a1
	move.w	#3,28(a1)
	move.l	a4,40(a1)
	move.l	#1,36(a1)
	move.l	ExecBase,a6
	jsr	DoIO(a6)
	movem.l	(a7)+,d0/a0-a1/a4/a6
	rts

Puffer:	dc.l	0,0

*
* ReadCrL
*
* a0 < Zeiger auf den Puffer f�r die gelesenen Daten
*

ReadCrL:moveq	#0,d1
RCLoop:	bsr	ReadChar

	cmp.b	#$9b,d0		; CSI
	beq	CSIKey

	add.l	#1,d1
	move.b	d0,(a0)+

	bsr	WriteChar

	cmp.b	#08,d0		; BackSpace
	bne	RCBra1
	lea	-2(a0),a0
	sub.l	#2,d1
	move.l	d0,-(a7)
	move.l	#" ",d0
	bsr	WriteChar
	move.l	#08,d0
	bsr	WriteChar
	move.l	(a7)+,d0

RCBra1:	cmp.b	#13,d0
	bne	RCLoop

	move.b	#0,(a0)+
	move.l	d1,d0
	rts

CSIKey:	bsr	ReadChar
	bra	RCLoop

*
* ReadStr
*
* a0 < Zeiger auf Text-Puffer
*

ReadStr
	bsr	ReadCrL
	move.b	#10,d0
	bsr	WriteChar
	rts

*
* WriteXY
*
* a0 < Zeiger auf Zeichenkette
* d0 < X-Position
* d1 < Y-Position
*

WriteXY:
	bsr	GotoXY

*
* WriteStr
*
* a0 < Zeiger auf Zeichenkette
*

WriteStr:
	movem.l	a2,-(a7)
	move.l	a0,a2
	move.l	#-1,d0
WSLoop:	add.l	#1,d0
	tst.b	(a2)+
	bne	WSLoop
	movem.l	(a7)+,a2

*
* WriteStr
*
* a0 < Zeiger auf Zeichenkette
* d0 < L�nge der Zeichenkette
*

WriteLeL:
	movem.l	a1/a6,-(a7)
	move.l	8(a5),a1
	move.w	#3,28(a1)	
	move.l	a0,40(a1)	
	move.l	d0,36(a1)	
	move.l	ExecBase,a6
	jsr	DoIO(a6)
	movem.l	(a7)+,a1/a6
	rts

*
* WirteDec
*
* d0 < Zahlenwert (2Byte)
*

WriteDec:
	movem.l	a1,-(a7)
	lea	HexPuffer,a0
	move.l	a0,a1
	bsr	HtDStr
	move.b	#0,(a0)
	move.l	a1,a0
	bsr	WriteStr
	movem.l	(a7)+,a1
	rts

*
* WirteHex
*
* d0 < Zahlenwert (2Byte)
*

WriteHex:
	movem.l	a1,-(a7)
	lea	HexPuffer,a0
	move.l	a0,a1
	bsr	HtHStr
	move.b	#0,(a0)
	move.l	a1,a0
	bsr	WriteStr
	movem.l	(a7)+,a1
	rts

HexPuffer:
	dc.l	0,0

*
* GotoXY
*
* d0 < X-Position
* d1 < Y-Position
*

GotoXY:
	movem.l	a0-a1,-(a7)
	lea	GXY_Text,a1
	lea	1(a1),a0
	exg	d1,d0
	bsr	HtDStr
	move.b	#$3b,(a0)+
	exg	d1,d0
	bsr	HtDStr
	move.b	#$48,(a0)+
	move.b	#$00,(a0)

	move.l	a1,a0
	bsr	WriteStr
	movem.l	(a7)+,a0-a1
	rts

GXY_Text:
	dc.b	$9b,0
	ds.l	4

HtDStr:	;	Hex to Dec String
	divu	#1000,d0
	bsr	HtDSBranch
	divu	#100,d0
	bsr	HtDSBranch
	divu	#10,d0
	bsr	HtDSBranch
HtDSBranch:
	add	#$30,d0
	move.b	d0,(a0)+
	clr.w	d0
	swap	d0
	rts



HtHStr:	;	Hex to Hex String
	movem.l	d1-d2,-(a7)
	move.l	#7,d2
HtHSLoop:
	move.l	d0,d1
	and.l	#$F0000000,d1
	bsr	HtHSBranch
	lsl.l	#4,d0
	dbf	d2,HtHSLoop
	movem.l	(a7)+,d1-d2
	rts

HtHSBranch:
	rol.l	#4,d1
	add.b	#"0",d1
	cmp	#"9",d1
	ble	HtHSOk
	add	#"A"-"9",d1
HtHSOk:	move.b	d1,(a0)+
	rts

*
* ScrollUp
*
* d0 < Zahl der Zeilen, die nah oben gescrollt werden sollen
*

ScrollUp:
	movem.l	a0-a1,-(a7)
	lea	Scroll_Text,a1
	lea	1(a1),a0
	bsr	HtDStr
	move.b	#$53,(a0)+
	move.b	#$00,(a0)+

	move.l	a1,a0
	bsr	WriteStr
	movem.l	(a7)+,a0-a1
	rts

*
* ScrollUp
*
* d0 < Zahl der Zeilen, die nah oben gescrollt werden sollen
*

ScrollDown:
	movem.l	a0-a1,-(a7)
	lea	Scroll_Text,a1
	lea	1(a1),a0
	bsr	HtDStr
	move.b	#$54,(a0)+
	move.b	#$00,(a0)+

	move.l	a1,a0
	bsr	WriteStr
	movem.l	(a7)+,a0-a1
	rts

Scroll_Text:
	dc.b	$9b,0
	ds.l	4

*
* DelLine
*

DelLine:
	moveq	#0,d0
	bra	DelEOLPrint

*
* InsLine
*

InsLine:
	moveq	#2,d0
	bra	DelEOLPrint

*
* DelEOD
*

DelEOD:
	moveq	#4,d0
	bra	DelEOLPrint

*
* DelEOL
*

DelEOL:
	moveq	#6,d0
DelEOLPrint:
	movem.l	a0,-(a7)
	lea	TextData,a0
	add.l	d0,a0
	moveq	#2,d0
	bsr	WriteLeL
	movem.l	(a7)+,a0
	rts

TextData:
DelLine_Text:	dc.w	$9b4d
InsLine_Text:	dc.w	$9b4c
DelEOD_Text:	dc.w	$9b4a
DelEOL_Text:	dc.w	$9b4b

*
* CursorOn
*

CursorOn:
	lea	CON_Text,a0
	move.l	#3,d0
	bra	WriteLeL

*
* CursorOff
*

CursorOff:
	lea	COFF_Text,a0
	move.l	#4,d0
	bra	WriteLeL

COFF_Text:	dc.b	$9b,$30,$20,$70
CON_Text:	dc.b	$9b,$20,$70,$00


EndResident:

; Ende der conio.library



*
*	Kapitel 10
*	Quelltext der "io.library"
*

ExecBase	=	4
OpenLib 	=	-552
CloseLib	=	-414
Output		=	-60
Write		=	-48
Remove		=	-252
FreeMem		=	-210
AllocMem	=	-198
AddPort		=	-354
RemPort		=	-360
AllocSignal	=	-330
FreeSignal	=	-336
FindPort	=	-390
OpenDevice	=	-444
CloseDevice	=	-450

Start:	move.l	ExecBase,a6	; DosLibrary �ffnen
	lea	DosName,a1
	moveq 	#0,d0
	jsr	OpenLib(a6)
	move.l	d0,a6
	beq	DosError

	jsr	Output(a6)	; Ausgabekanal ermitteln
	move.l	d0,d1

	move.l	#TextA,d2
	move.l	#TextE-TextA,d3
	jsr	Write(a6)
 
	move.l	a6,a1
	move.l	ExecBase,a6
	jsr	CloseLib(a6)

DosError:
	moveq  #0,d0
	rts

TextA:	dc.b	10,"�� IO-Library Version 0.1 (Ron) ��",10,10
TextE:
	even

LibResident:
	dc.w	$4AFC		; Resident-Struktur
	dc.l	LibResident
	dc.l	EndResident
	dc.b	%10000000
	dc.b	1
	dc.b	9
	dc.b	0
	dc.l	LibName
	dc.l	LibIDString
	dc.l	LibInitData

LibName:	dc.b	"io.library",0
	even
LibIDString:	dc.b	"IO-Library v0.1",0
	even

LibInitData:
	dc.l	38	; LibSize (34) + SegList (4) = 38
	dc.l	FuncTab
	dc.l	DataTab
	dc.l	LibInitRout

FuncTab:
	dc.l	Open		;-6
	dc.l	Close		;-12 Standardfunktionen
	dc.l	Expunge		;-18
	dc.l	ExtFunc		;-24

	dc.l	CreatePort	;-30
	dc.l	DeletePort	;-36 Spezialfunktionen
				;
	dc.l	CreateIOReq	;-42
	dc.l	DeleteIOReq	;-48
				;
	dc.l	CreateIOFast	;-54
	dc.l	DeleteIOFast	;-60

	dc.l	-1

DataTab:
	dc.b	%11100000,0	; Tabelle f�r InitStruct
	dc.w	8
	dc.b	9,0

	dc.b	%11000000,0
	dc.w	10
	dc.l	LibName

	dc.b	%11100000,0
	dc.w	14
	dc.b	6,0

	dc.b	%11000000,0
	dc.w	20
	dc.w	3,4

	dc.l	0

DosName:	dc.b "dos.library",0
	even


; Initialisierungsroutine

LibInitRout:
	movem.l	a5,-(a7)

	move.l	d0,a5		; Library-Basis nach a4

	move.l	a0,34(a5)	; Segment-Liste eintragen

LibInitEnd:
	movem.l	(a7)+,a5
	rts

; Open-Routine

Open:	
	bclr	#3,14(a6)	; ExpungBit = 0
	addq.w	#1,32(a6)	; OpenCnt ++
	move.l	a6,d0
	rts

; Close-Routine

Close:	moveq	#0,d0

	subq.w	#1,32(a6)	; OpenCnt --
	bne	CloseEnd

	btst	#3,14(a6)	; ExpungBit = 1 ?
	beq	CloseEnd

	bsr	Expunge		; entfernen

CloseEnd:
	rts

; Expunge-Routine

Expunge:
	movem.l	d1-d2/a1-a6,-(a7)

	tst.w	32(a6)		; OpenCnt = 0 ?
	beq	ExpungeBranch

	moveq	#0,d0		; Nein:
	bset	#3,14(a6)	
	bra	ExpungeEnd

ExpungeBranch:
	move.l	a6,a4		; Ja:
	move.l	ExecBase,a6
	move.l	a4,a1		; Lib aus Liste nehmen
	jsr	Remove(a6)	; Remove

	move.l	34(a4),d2	; SegmentList retten

	moveq	#0,d0		; Library-Speicher freigeben
	move.w	16(a4),d0
	move.l	a4,a1
	sub.l	d0,a1
	add.w	18(a4),d0
	jsr	FreeMem(a6)	; FreeMem

	move.l	d2,d0		; SegmentList zur�ckgeben

ExpungeEnd:
	movem.l	(a7)+,d1-d2/a1-a6
	rts

; ExtFunc-Routine

ExtFunc:
	moveq	#0,d0		; R�ckgabewert = Null
	rts

; 
; Nun folgen die sechs Spezialfunktionen, die die Kommunikation mit
; Devices erleichtern sollen:
;

*
*	CreatePort
*
* a0 < Zeiger auf Port-Namen
* d0 < Priorit�t des Ports
*
* d0 > Zeiger auf erstellte Port-Struktur
*

; Die CreatePort-Routine kennen wir schon aus dem Exec-Kapitel

CreatePort:
	movem.l	d1-d3/a1-a3/a6,-(a7)	; Register retten
	move.l	ExecBase,a6
	move.l	d0,d3
	move.l	a0,a2		; Parameter sichern

	move.l	a0,a1
	jsr	FindPort(a6)	; Kontrollieren, ob es einen
	tst.l	d0		; Port mit dem angegebene Namen
	bne	CPError		; schon gibt

	move.l	#34,d0
	move.l	#$10000,d1
	jsr	AllocMem(a6)	; Speicher f�r MsgPort-Struktur
	move.l	d0,a3		; belegen
	beq	CPError

	move.l	#-1,d0
	jsr	AllocSignal(a6)	; Signal belegen
	tst.b	d0
	bmi	DPFreeMem

	move.b	#4,8(a3)	; MsgPort-Struktur einrichten
	move.b	d3,9(a3)
	move.b	#0,14(a3)
	move.b	d0,15(a3)
	move.l	a2,10(a3)
	move.l	276(a6),16(a3)

	move.l	a3,a1		; Wenn ein Namen angegeben wurde, wird
	tst.l	10(a1)		; der Port in die PublicPort-Liste 
	beq	CPBranch	; aufgenommen
	jsr	AddPort(a6)
	bra	CPOK

CPBranch:
	lea	20(a3),a1	; Sollte der Port nicht in die PP-Liste
	move.l	a1,(a1)		; aufgenommen werden, mu� man den Listenkopf
	addq.l	#4,(a1)		; der Nachrichten (MsgList) von "Hand"
	clr.l	4(a1)		; einrichten !!! Sonst h�tte das AddPort
	move.l	a1,8(a1)	; f�r uns erledigt !

CPOK:	move.l	a3,d0		; Zeiger auf Port �bergeben
CPError:
	movem.l	(a7)+,d1-d3/a1-a3/a6	; Pop
	rts

*
*	DeletePort
*
* a0 < Zeiger auf Port-Struktur, die aufgel�st werden soll
*

; Auch die DeletePort-Funktion kennen wir aus dem Exec-Kapitel

DeletePort:
	movem.l	d1-d3/a1-a3/a6,-(a7)
	move.l	ExecBase,a6
	move.l	a0,a3		; Parameter retten

	move.l	a3,a1		; Hat der Port einen Namen, so ist er in die
	tst.l	10(a1)		; PP-Liste eingetragen worden. Jetzt m�ssen
	beq	DPBranch	; wir ihn wieder entfernen.
	jsr	RemPort(a6)
DPBranch:

	move.b	15(a3),d0	; Signal freigeben
	ext.w	d0
	ext.l	d0
	jsr	FreeSignal(a6)

DPFreeMem:
	move.l	#34,d0
	move.l	a3,a1
	jsr	FreeMem(a6)	; Speicher freigeben
	
	movem.l	(a7)+,d1-d3/a1-a3/a6
	moveq	#0,d0		; Null �bergeben
	rts

*
*	CreateIOReq
*
* a0 < Zeiger auf den Reply-Port
* d0 < Gr��e der Nachrichtenstruktur
*
* d0 > Zeiger auf die eingerichtete IO-Request-Struktur
*

CreateIOReq:
	movem.l	d1/a6,-(a7)
	move.l	d0,d3		; Parameter retten
	move.l	a0,a3

	move.l	ExecBase,a6	; Speicherbereich mit angegebener Gr��e
	move.l	#$10000,d1	; belegen
	jsr	AllocMem(a6)
	move.l	d0,a0
	tst.l	d0
	beq	CIORError

	move.l	a3,14(a0)	; Zeiger auf Reply-Port eintragen
	move.w	d3,18(a0)	; und L�nge der Nachricht eintragen

CIORError:
	movem.l	(a7)+,d1/a6
	rts

*
*	DeleteIOReq
*
* a0 < Zeiger auf den IO-Request-Struktur
*

DeleteIOReq:
	movem.l	d1/a6,-(a7)
	move.l	ExecBase,a6

	moveq	#0,d0		; Speicherbereich wieder freigeben
	move.w	18(a0),d0
	move.l	a0,a1
	jsr	FreeMem(a6)

	movem.l	(a7)+,d1/a6
	moveq	#0,d0		; R�ckgabewert = Null
	rts
	

*
*	CreateIOFast
*
* a0 < Zeiger auf Device Name
* d0 < UnitNumber
* d1 < Flags
* d2 < L�nge der IORequest-Sturktur
*
* d0 > Zeiger auf erstellte IOReq-Struktur
*

CreateIOFast:
	movem.l	d3-d6/a1-a6,-(a7) ; Push

	movem.l	a0/d0-d1,-(a7)	; Parameter in h�here Register
	movem.l	(a7)+,a5/d5-d6	; kopieren

	move.l	ExecBase,a6	; Reply-Port erstellen
	sub.l	a0,a0		;  Keinen Namen
	moveq	#0,d0		;  Priorit�t = 0
	bsr	CreatePort
	move.l	d0,a0
	beq	CIOFError0

	move.l	a0,a3		; IORequest-Struktur einrichten
	move.l	d2,d0		;  ReplyPort
	bsr	CreateIOReq	;  Gr��e
	move.l	d0,a3		;  IORequest sichern
	beq	CIOFError1

				; Device �ffnen
	move.l	a5,a0		;  Zeiger auf Namen
	move.l	a3,a1		;  Zeiger auf IORequest
	move.l	d5,d0		;  Unit
	move.l	d6,d1		;  Flags
	jsr	OpenDevice(a6)
	tst.l	d0
	bne	CIOFError2

	move.l	a3,d0		; Zeiger auf IORequest-Stuktur �bergeben

CIOFError0:
	movem.l	(a7)+,d3-d6/a1-a6
	rts

*
*	DeleteIOFaste
*
* a0 < Zeiger auf IOReq-Struktur
*

DeleteIOFast:
	movem.l	d3-d6/a1-a6,-(a7)

	move.l	ExecBase,a6

	move.l	a0,a3		; Device sclhie�en
	move.l	a0,a1
	jsr	CloseDevice(a6)

CIOFError2:
	move.l	a3,a0		; IO-Request-Struktur freigeben
	move.l	14(a0),a3
	bsr	DeleteIOReq

CIOFError1:
	move.l	a3,a0		; Reply-Port freigeben
	bsr	DeletePort

DIOFError:
	movem.l	(a7)+,d3-d6/a1-a6
	moveq	#0,d0		; Fehlercode = 0
	rts

EndResident:

; Ende der io.library


ExecBase	=	4
CreateIOFast	=	-54
DeleteIOFast	=	-60
OpenLib		=	-552
CloseLib	=	-414
DoIO		=	-456
SendIO		=	-462
CheckIO		=	-468
Write		=	-48
Output		=	-60


Start:
	move.l	ExecBase,a6
	moveq	#0,d0
	lea	IOName,a1
	jsr	OpenLib(a6)
	move.l	d0,IOBase
	beq	IOError

	moveq	#0,d0
	lea	DosName,a1
	jsr	OpenLib(a6)
	move.l	d0,DosBase
	beq	DosError

	move.l	ExecBase,a6
	lea	DosName,a1
	moveq	#0,d0
	jsr	OpenLib(a6)
	move.l	d0,DosBase
	beq	Error1

	move.l	IOBase,a6
	lea	DevName,a0
	move.l	#2,d0
	moveq	#0,d1
	move.l	#36,d2
	jsr	CreateIOFast(a6)
	tst.l	d0
	beq	Error2
	move.l	d0,IOReq

	move.l	ExecBase,a6
	move.l	IOReq,a1
	move.w	#9,28(a1)
	move.l	#MySub,32(a1)
	jsr	SendIO(a6)

	move.l	DosBase,a6
	jsr	Output(a6)
	move.l	d0,d7

Loop:	move.l	DosBase,a6
	move.l	d7,d1
	move.l	#Text,d2
	move.l	#TextE-Text,d3
	jsr	Write(a6)

	move.l	ExecBase,a6
	move.l	IOReq,a1
	jsr	CheckIO(a6)
	tst.l	d0
	beq	Loop

	move.l	IOBase,a6
	move.l	IOReq,a0
	move.l	20(a0),a1	; io_Device
	bset	#3,14(a1)	; Expunge-Bit setzen
	jsr	DeleteIOFast(a6)

Error2:	move.l	ExecBase,a6
	move.l	DosBase,a6
	jsr	CloseLib(a6)

Error1:
	move.l	ExecBase,a6
	move.l	DosBase,a1
	jsr	CloseLib(a6)

DosError:
	move.l	ExecBase,a6
	move.l	IOBase,a1
	bset	#3,14(a1)	; Expunge-Bit setzen
	jsr	CloseLib(a6)

IOError:moveq	#0,d0
	rts

MySub:	move.l	#$40000,d0
MyLoop	move.w	d0,$dff180
	sub.l	#1,d0
	bne	MyLoop
	rts

DevName:dc.b	"test.device",0
	even
IOName:	dc.b	"io.library",0
	even
DosName:dc.b	"dos.library",0
	even
Text:	dc.b	"Device-Task bearbeitet noch die Routine",10
TextE:	even
IOBase:	dc.l	0
DosBase:dc.l	0
IOReq:	dc.l	0


*
*	Kapitel 11
*	Quelltext des test.device
*

ExecBase	=	4
Output		=	-60
Write		=	-48
OpenLib		=	-552
CloseLib	=	-414
Remove		=	-252
FreeMem		=	-210
AllocMem	=	-198
WaitPort	=	-384
GetMsg		=	-372
PutMsg		=	-366
ReplyMsg	=	-378
AddTask		=	-282
RemTask		=	-288

Start:	move.l	ExecBase,a6	; Dos-Library �ffnen
	lea	DosName,a1
	moveq 	#0,d0
	jsr	OpenLib(a6)
	move.l	d0,a6
	beq	DosError

	jsr	Output(a6)	; Ausgabekanal ermitteln
	move.l	d0,d1

	move.l	#TextA,d2	; Text ausgeben
	move.l	#TextE-TextA,d3
	jsr	Write(a6)
 
	move.l	a6,a1		; DosLibrary schlie�en
	move.l	ExecBase,a6
	jsr	CloseLib(a6)

DosError:
	moveq  #0,d0
	rts

TextA:	dc.b	10,"�� TestDevice Version 0.1 (Ron) ��",10,10
TextE:
	even

; Nun folgt der eigentliche Teil des Device

LibResident:
	dc.w	$4AFC		; (Illegal)
	dc.l	LibResident
	dc.l	EndResident
	dc.b	%10000000
	dc.b	1
	dc.b	3
	dc.b	0
	dc.l	DevName
	dc.l	LibIDString
	dc.l	LibInitData

DevName:	dc.b	"test.device",0
	even
LibIDString:	dc.b	"TestDevice v0.1",0
	even

LibInitData:
	dc.l	50	; LibSize (34) + SegList (4) + MsgPort (4) + TaskPtr (4) + Message (4) = 50
	dc.l	FuncTab
	dc.l	DataTab
	dc.l	LibInitRout

FuncTab:
	dc.l	Open		;
	dc.l	Close		; Library-Standardfunktionen
	dc.l	Expunge		;
	dc.l	ExtFunc		;

	dc.l	BeginIO		; Device-Standardfunktionen
	dc.l	AbortIO		;

	dc.l	-1

DataTab:
	dc.b	%11100000,0	; Datentabelle f�r InitStruct
	dc.w	8		
	dc.b	3,0		

	dc.b	%11000000,0
	dc.w	10
	dc.l	DevName

	dc.b	%11100000,0
	dc.w	14
	dc.b	6,0

	dc.b	%11000000,0
	dc.w	20
	dc.w	3,4

	dc.l	0

DosName:	dc.b "dos.library",0
	even


; Initialisierungsroutine:

LibInitRout:
	movem.l	d0-d6/a0-a6,-(a7)

	move.l	d0,a5		; Library-Basis nach a5
	move.l	d0,LibPtr
	move.l	a0,34(a5)	; Segment-Liste eintragen
	move.l	#MsgPort,38(a5)	; Zeiger auf MsgPort eintragen

	move.l	ExecBase,a6	; Stackspeicher belegen
	move.l	#600,d0
	moveq	#0,d1
	jsr	AllocMem(a6)
	move.l	d0,tc_SPLower	; Stackpointer eintragen
	add.l	#600,d0
	move.l	d0,tc_SPUpper
	move.l	d0,tc_SPReg

	move.l	#TaskStruktur,a1
	move.l	a1,42(a5)
	move.l	a1,mp_SigTask
	move.l	#DevTask,a2
	sub.l	a3,a3
	jsr	AddTask(a6)	; Device-Task eintragen

	move.l	LibPtr,d0

LibInitEnd:
	movem.l	(a7)+,d0-d6/a0-a6
	rts

; Open-Routine

Open:	
	bclr	#3,14(a6)	; ExpungBit = 0
	addq.w	#1,32(a6)	; OpenCnt ++
	move.l	a6,d0
	rts

; Close-Routine

Close:	movem.l	a2-a3/a5,-(a7)
	moveq	#0,d0
	move.l	#-1,20(a1)	; io_Device l�schen
	move.l	#-1,24(a1)	; io_Unit l�schen

	subq.w	#1,32(a6)	; OpenCnt --
	bne	CloseEnd

	btst	#3,14(a6)
	beq	CloseEnd

	bsr	Expunge		; entfernen

CloseEnd:
	movem.l	(a7)+,a2-a3/a5
	rts

; Expunge-Routine

Expunge:
	movem.l	d1-d2/a1-a6,-(a7)

	tst.w	32(a6)
	beq	ExpungeBranch

	moveq	#0,d0		; SegmentList = 0
	bset	#3,14(a6)	; ExpungeBit setzen
	bra	ExpungeEnd

ExpungeBranch:
	move.l	a6,a5		; Device aus Device-Liste entfernen
	move.l	ExecBase,a6
	move.l	a5,a1
	jsr	Remove(a6)	; Remove

	lea	TaskStruktur,a1
	jsr	RemTask(a6)	; Task entfernen

	move.l	tc_SPLower,a1
	move.l	#600,d0
	jsr	FreeMem(a6)	; Stackspeicher freigeben

	move.l	34(a5),d2	; SegmentList retten

	moveq	#0,d0
	move.w	16(a5),d0
	move.l	a5,a1
	sub.l	d0,a1
	add.w	18(a5),d0	; Library-Struktur freigeben
	jsr	FreeMem(a6)	; FreeMem

	move.l	d2,d0		; SegmentList zur�ckgeben

ExpungeEnd:
	movem.l	(a7)+,d1-d2/a1-a6
	rts

; ExtFunc-Routine

ExtFunc:
	moveq	#0,d0
	rts

; BeginIO-Routine

BeginIO:
	movem.l	d1-d6/a0-a6,-(a7)

	clr.b	31(a1)		; Error l�schen
	bclr	#0,30(a1)	; Quick Bit l�schen

	move.l	38(a6),a0	; Zeiger auf MsgPort auslesen
	move.l	ExecBase,a6
	jsr	PutMsg(a6)	; Nachricht abschicken

	moveq	#0,d0

	movem.l	(a7)+,d1-d6/a0-a6
	rts

; AbortIO-Routine

AbortIO:
	moveq	#0,d0
	rts

;
; --- Device-Task --------------------------------
;

DevTask:
	move.l	LibPtr,a5	; Zeiger auf Basisadresse der Lib nach a5

MessageLoop:
	move.l	ExecBase,a6
	move.l	38(a5),a0
	jsr	WaitPort(a6)	; auf Nachrichten warten
	move.l	d0,46(a5)

	lea	MsgPort,a0
	jsr	GetMsg(a6)	; Nachricht entfernen
	tst.l	46(a5)
	beq	MessageLoop

; Nachrichtenbehandlung

	move.l	46(a5),a4
	moveq	#0,d0
	move.w	28(a4),d0	; io_Command auslesen
	lsl.l	#1,d0		; * 2
	lea	StartCMD,a0	; Zeiger auf Tabelle
	move.w	(a0,d0),d0	; Offset auslesen
	jsr	(a0,d0)		; Routine aufrufen

	move.l	46(a5),a1
	jsr	ReplyMsg(a6)	; Nachricht best�tigen
	bra	MessageLoop

StartCMD
	dc.w	CMDInvalid-StartCMD	; 0
	dc.w	CMDReset-StartCMD	; 1
	dc.w	CMDRead-StartCMD	; 2
	dc.w	CMDWrite-StartCMD	; 3
	dc.w	CMDUpDate-StartCMD	; 4
	dc.w	CMDClear-StartCMD	; 5
	dc.w	CMDStop-StartCMD	; 6
	dc.w	CMDStart-StartCMD	; 7
	dc.w	CMDAbort-StartCMD	; 8

	dc.w	CMDExecuteSub-StartCMD	; 9

CMDInvalid:
CMDReset:
CMDRead:
CMDWrite:
CMDUpDate:
CMDClear:
CMDStop:
CMDStart:
CMDAbort:
	rts

CMDExecuteSub:
	move.l	46(a5),a0		; ExeSub-Funktion
	move.l	32(a0),a0
	movem.l	a0-a6/d0-d7,-(a7)
	jsr	(a0)
	movem.l	(a7)+,a0-a6/d0-d7
	rts

* Datenbereich

LibPtr:		dc.l	0	; Speicherbereich f�r Base-Pointer

MsgPort:	dc.l	0,0	; Message-Port-Struktur
		dc.b	4,0
		dc.l	DevName
		dc.b	0,24
mp_SigTask	dc.l	0
mp_Head		dc.l	mp_Tail	; Message-Port-List mu� 
mp_Tail		dc.l	0	; initialisiert sein
mp_TailPred	dc.l	mp_Head
		dc.b	0,0

TaskStruktur:	dc.l	0,0	;
		dc.b	1,0	; Node-Struktur
		dc.l	DevName	;

		dc.b	0	;
		dc.b	0	;
		dc.b	0	;
		dc.b	0	;
		dc.l	0	;
		dc.l	0	;
		dc.l	0	;
		dc.l	0	;
		dc.w	0	;
		dc.w	0	;
		dc.l	0	;
		dc.l	0	;
		dc.l	0	;
		dc.l	0	;
tc_SPReg:	dc.l	0	;
tc_SPLower:	dc.l	0	;
tc_SPUpper:	dc.l	0	;
		dc.l	0	;
		dc.l	0	;
		dcb.b	14	;
		dc.l	0	;

EndResident:

; Ende des Devices


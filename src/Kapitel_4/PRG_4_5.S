
* Programm 4.5: Ein- und Ausgabe in ein CON-Fenster

ExecBase	=	4
OpenLib	=	-552
CloseLib	=	-414
Open	=	-30
Write	=	-48
Read	=	-42
Close	=	-36

	move.l	ExecBase,a6	; DOS-Lib �ffnen
	lea	dosname,a1
	clr.l	d0
	jsr	OpenLib(a6)
	move.l	d0,a6

* CON-Fenster �ffnen

	move.l	#conname,d1	; CON-Dateiname nach d1
	move.l	#1005,d2	; Ger�t schon vorhanden
	jsr	Open(a6)	; 'Datei' �ffnen
	tst.l	d0	; Fehler beim �ffnen?
	beq	ende	; Wenn ja
	move.l	d0,d4	; Handle in d4 sichern

	lea	text1,a0	; Text im Fenster ausgeben
	bsr	print

* Text von Tastatur lesen

	move.l	d4,d1	; CON-Handle
	move.l	#buff,d2	; Startadresse Textbuffer
	move.l	#40,d3	; Max. 40 Zeichen
	jsr	Read(a6)	; Von CON lesen
	move.l	d0,d3	; L�nge sichern

* Gelesenen Text wieder ausgeben:

	lea	text2,a0	; Einleitungs-Text
	bsr	print

	lea	buff,a0	; Pufferstart
	move.b	#0,0(a0,d0)	; Text mit Nullbyte abschlie�en
	bsr	print	; Und raus damit

* Auf Return warten

	move.l	d4,d1	; CON-Handle
	move.l	#buff,d2	; Pufferstart
	move.l	#1,d3	; Eingabetext ist unwichtig
	jsr	Read(a6)	; Lesen

* CON-Fenster schlie�en

	move.l	d4,d1	; Handle
	jsr	Close(a6)	; 'Datei' schlie�en

ende:	move.l	a6,a1	; Lib schlie�en
	move.l	ExecBase,a6
	jsr	CloseLib(a6)
	rts

print:	movem.l	d1-d3,-(sp)	; SUB Textausgabe f�r DOS-Write
	move.l	a0,d2	; *a0 < Zeiger auf Text (0-terminiert)
	clr.l	d3	;  d4 < Handle der Ausgabedatei
pr1:	addq	#1,d3
	tst.b	(a0)+
	bne	pr1
	subq	#1,d3

	move.l	d4,d1
	jsr	Write(a6)
	movem.l	(sp)+,d1-d3
	rts

* Datenbereich

dosname:	dc.b	"dos.library",0
	even
conname:	dc.b	"con:0/0/640/60/Ein CON-Fenster",0
	even
text1:	dc.b	"Geben Sie einen Text ein: ",0
	even
text2:	dc.b	"Sie haben eingegeben: ",0
	even
buff:	ds.b	40
